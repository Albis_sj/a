import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Libro } from 'src/app/models/libro.model';
import { Usuario } from 'src/app/models/usuario.model';
import { LibroService } from 'src/app/servicios/libro.service';
import { UsuarioService } from 'src/app/servicios/usuario.service';

@Component({
  selector: 'app-un-libro',
  templateUrl: './un-libro.component.html',
  styleUrls: ['./un-libro.component.css'],
})
export class UnLibroComponent implements OnInit {
  id!: number;
  readLibro!: Libro;
  telefono!: number | undefined;
  link: string = 'https://wa.me/+591'
  usuario!: Usuario;

enlace: string = '';
  constructor(
    private usuarioService: UsuarioService,
    private libroService: LibroService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.activatedRoute.params.subscribe((params) => {
      let id = params['id'];

      if (id !== undefined) {
        console.log('id !== undefined');
        this.libroService.obtenerLibroporId(id).subscribe({
          next: (res) => {
            console.log(res);
            this.readLibro = res;
            this.telefono = res.usuario.celular
            console.log(this.telefono);
            this.link = this.link + this.telefono + '?text=Buenas,%20estoy%20interesado%20en%20el%20libro%20' + res.titulo;
            console.log(this.link);
            70034333
            
            
          },
          error: () => {

          }
        });
      } else {
        console.log('EROOOOR');
      }
    });
  }

  ngOnInit(): void {
    
  }

  // Getvendedor(id: number) {
  //   this.usuarioService.getOneData(this.id).subscribe((res) => {
  //     this.vendedor = res.data[0];
  //     console.log(this.vendedor, 'this.vendedor');
  //   });
  // }
}
