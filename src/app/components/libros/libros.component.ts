import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Categoria } from 'src/app/models/categoria.model';
import { CategoriaService } from 'src/app/servicios/categoria.service';
import { LibroService } from 'src/app/servicios/libro.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css'],
})
export class LibrosComponent implements OnInit {
  formato: any;
  public page: number = 0;
  public number: number = 1;
  pageActual: number = 1;
  termino!: string;
  libros: any[] = [];
  mostrarBuscador: boolean = true;
  mostrarFiltro: boolean = false;
  propiedades: any = {
    error: true
  }
  constructor(
    private libroService: LibroService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private categoriaService: CategoriaService
  ) {
    this.Categoria();
    this.Libros();
    this.Formato();
    // this.activatedRoute.params.subscribe(params => { //subscribe obtiene una variable
    //   console.log(params['termino'], 'Init del termino');
    //   this.termino = params['termino']; //se lo guarda en una variables lo que hace log, y esa variable 
    //   console.log(this.termino, 'termino = params');
    //  if(this.termino !== undefined){
    //   this.libroSVC.getBuscar(this.termino.toLowerCase()).subscribe((res)=>{
    //     this.libros = res.data
    //     console.log(this.libros);
    //   })
    //  } else {
    //   console.log('undefined,, ');
    //   this.mostrarBuscador = false
    //  }
    //   console.log(this.libros, 'lista de libros');
    // })
  }

  ngOnInit(): void {}

  public readCategoria: any;
  public readData: any;
  public readFiltro: any;
  readFormato: any;

  Categoria() {
    this.categoriaService
      .cargarCategorias2()
      .subscribe((categorias: Categoria[]) => {
        this.readCategoria = categorias;
      });
  }


  Libros() {
    this.libroService.cargarLibros().subscribe({
      next: (resp) => {
        console.log(resp);
        this.readData = resp;
      },
      error: (err) => {
        console.log(err.errors.msg);
      },
    });
  }

  Formato() {
    this.formato = this.libroService.formato;
  }


  buscarLibro(termino: string): void {
    this.router.navigate(['/buscar', termino]);
  }


  VerLibro(id: any) {
    this.router.navigate(['/libro', id]);
  }

  getLibroCategoria(id: number) {
    this.libroService.FiltrarLibrosPor().subscribe((res: any)=>{
    this.readFiltro = res.libros;
    this.mostrarFiltro = true;
  });
}

}

// this.activatedRoute.params.subscribe(params => { //subscribe obtiene una variable
//   console.log(params['termino'], 'Init del termino');
//   this.termino = params['termino']; //se lo guarda en una variables lo que hace log, y esa variable
//   console.log(this.termino, 'termino = params');
//  if(this.termino !== undefined){
//   this.libroSVC.getBuscar(this.termino.toLowerCase()).subscribe((res)=>{
//     this.libros = res.data
//     console.log(this.libros);
//   })
//  } else {
//   console.log('undefined,, ');
//   this.mostrarBuscador = false
//  }
//   console.log(this.libros, 'lista de libros');
// })



// getLibroFormato(id: string){
//   this.readFiltro = this.libroService.formato;
//   this.mostrarFiltro = true;

//   this.libroSVC.getLibroFormato(id).subscribe((res)=>{
//     this.readFiltro = res.data;
//     console.log('filtro Categoria 000',this.readFiltro, 'filtro CAtegoria 000');
//   });
// }