import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentsComponent } from './components.component';
import { EditorialesComponent } from './editoriales/editoriales.component';
import { InicioComponent } from './inicio/inicio.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { AsociadosComponent } from './asociados/asociados.component';
import { ComunicadosComponent } from './comunicados/comunicados.component';
import { ContactoComponent } from './contacto/contacto.component';
import { LibrosComponent } from './libros/libros.component';
import { EventosComponent } from './eventos/eventos.component';
import { UnLibroComponent } from './un-libro/un-libro.component';

const routes: Routes = [
  {
    path: '',
    component: ComponentsComponent,
    children: [
      {path: '', component: InicioComponent},
      {path: 'nosotros', component: NosotrosComponent},
      {path: 'editoriales', component: EditorialesComponent},
      {path: 'editoriales/:id', component: EditorialesComponent},
      {path: 'eventos', component: EventosComponent},
      {path: 'comunicados', component: ComunicadosComponent},
      {path: 'asociados', component: AsociadosComponent},
      {path: 'contacto', component: ContactoComponent},
      {path: 'libros', component: LibrosComponent},
      {path: 'libros/:termino', component: LibrosComponent},
      {path: 'libro/:id', component: UnLibroComponent},
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
