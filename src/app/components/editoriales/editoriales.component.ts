import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Libro } from 'src/app/models/libro.model';
import { Usuario } from 'src/app/models/usuario.model';
import { LibroService } from 'src/app/servicios/libro.service';
import { UsuarioService } from 'src/app/servicios/usuario.service';

@Component({
  selector: 'app-editoriales',
  templateUrl: './editoriales.component.html',
  styleUrls: ['./editoriales.component.css']
})
export class EditorialesComponent implements OnInit {

  lista: Usuario[] = [];
  libros: Libro[] = [];
  mostrar: boolean = false;

  constructor(private usuarioService: UsuarioService,
    private libroService: LibroService){

  }

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  cargarUsuarios() {
    this.usuarioService
      .cargarUsuarios()
      //aca esta desestructurando la respuesta
      .subscribe(({ total, usuarios }) => {
        console.log(usuarios);
        this.lista = usuarios;
        // this.totalUsuarios = total;
        // this.usuarios = usuarios;
        // this.usuariosTemp = usuarios;
        
      });
  }

  ObtenerLibros(id: any){
    this.mostrar = true;
    this.libroService.obtenerLibrosEditorial(id)
      .subscribe({
        next: (resp: any) => {
          console.log(resp);
          
          this.libros = resp;
          console.log(this.libros);
          
        },
        error: (error) => {
          console.log(error);
        }
      })
  }

}

  
//   ListaConFiltro:VendedorI[] = [];

//   // LIBRO-EDITORIAL
//   id!: number;
//   vendedor: VendedorI = {
//     nombre: '',
//     id_vendedor: 0,
//     categoria: '',
//     correo1: '',
//     correo2: 0,
//     contrasenia: '',
//     telefono1: '',
//     telefono2: '',
//     celular1: 0,
//     celular2: '',
//     direccion: '',
//     pagina: ''
//     };
//     readLibros:any = [];
//     mostrar_libro:boolean = true;
//   // LIBRO-EDITORIAL

//   constructor(private _usuarioSVC : UsuarioService,
//               private _librosSVC: LibroService,
//               private router: Router,
//               private activatedRoute: ActivatedRoute) {
//       this.getAll();

//       this.activatedRoute.params.subscribe(params => {
//         let id = params['id']
//         console.log(id);
        
//         //NO PASA DE AQUI
//         if(id !== undefined){
//           console.log('id !== undefined');
//           this.id = parseInt(id);

//         this._usuarioSVC.getOneData(this.id).subscribe((res)=>{
//           this.vendedor = res.data[0]
//           console.log(this.vendedor);
          
//         });

//         this._librosSVC.getLibroEdi(this.id).subscribe((res)=>{
//           this.readLibros = res.data;
//           console.log(this.readLibros, 'read Libros');

//           // console.log(this.mensaje, '000');
//         });
        
//       } else {
//         console.log('else');
//         this.mostrar_libro = false
//        }

//         console.log('lista de libros');
//       })


//     }

//     readData:any;

//   ngOnInit(): void {
//   }

//   getAll(){
//     this._usuarioSVC.getAllData().subscribe((res)=>{

//       // console.log(res, "res==> Vendedor");
//       this.readData = res.data;

//       for (let i = 0; i < this.readData.length; i++){
//         if(this.readData[i].categoria === 'Editorial'){

//           this.ListaConFiltro.push(this.readData[i])
//           // console.log('lista de editoriales' + this.ListaConFiltro);
//         }
//       }
//     });
//   }

//   VerLibroEdi(id_vendedor: number){
//     this.router.navigate(['/editoriales', id_vendedor]);
//   }

//   buscarLibro(termino: string): void{
//     this.router.navigate(['/buscar', termino]); //al apretar enter o el boton, aqui ira al componente de buscar, y el termino como pusimos en routes, y entra a la clase buscadorComponente
//   }



// }
