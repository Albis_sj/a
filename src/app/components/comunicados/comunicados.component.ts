import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
// import { comunicadoI } from 'src/app/admin/add-comunicado/add-comunicado.component';
import { AdminService } from 'src/app/servicios/admin.service';

interface Item {
  imageSrc: string;
  imageAlt: string
}

@Component({
  selector: 'app-comunicados',
  templateUrl: './comunicados.component.html',
  styleUrls: ['./comunicados.component.css']
})
export class ComunicadosComponent implements OnInit {
  propiedades: any = {
    error: true
  }
  @Input() galleryData: Item[] = []
  // @ViewChild('asImage') image!: ElementRef;
  
  apiUrl = 'https://b190-186-121-194-116.sa.ngrok.io/addcomunicado';
  readData: comunicadoI[] = [
    {
        "id_addComunicado": 1,
        "titulo": "Comunicado Cambio de Nombre",
        "nombre_archivo": "LIBROS_yiaaaa",
        "descripcion": "Similique delectus error praesentium quidem eligendi dolorem, \ntemporibus quaerat, sapiente atque vitae odit animi nemo? Voluptatem adipisci doloremque eveniet amet soluta! Facere odio iste",
        "imagen1": "../../../../assets/img/comunicados/LIBROS_yiaaaa",
        "tipo1": ".pdf",
        "imagen2": "undefined",
        "tipo2": "undefined"
    },
    {
        "id_addComunicado": 2,
        "titulo": "8CHO VENDRÁ A LATINOAMERICA?",
        "nombre_archivo": "ochito",
        "descripcion": "Este es un comunicado pra que todos vayan al canal de youtube de 8cho \"8choplay \" y le digan que venga a latinoamerica, para que yo le vea, porque es mi sueño",
        "imagen1": "../../../../assets/img/comunicados/ochito.png",
        "tipo1": ".png",
        "imagen2": "undefined",
        "tipo2": "undefined"
    },
    {
        "id_addComunicado": 8,
        "titulo": "Comunicado 5",
        "nombre_archivo": "WhatsApp Image 2022-10-04 at 13",
        "descripcion": "Aca habia como un vacio legal como que el gobierno esta metido",
        "imagen1": "../../../../assets/img/comunicados/WhatsApp Image 2022-10-04 at 13.02.50.jpeg",
        "tipo1": ".jpeg",
        "imagen2": "undefined",
        "tipo2": "undefined"
    },
    {
        "id_addComunicado": 9,
        "titulo": "Prueba de descarga",
        "nombre_archivo": "WhatsApp Image 2022-10-04 at 13",
        "descripcion": "muy fuerte muy fuerte, esto claramente es una mano negra contra mi, contra la productora",
        "imagen1": "../../../../assets/img/comunicados/WhatsApp Image 2022-10-04 at 13.02.47.jpeg",
        "tipo1": ".jpeg",
        "imagen2": "undefined",
        "tipo2": "undefined"
    },
    {
        "id_addComunicado": 10,
        "titulo": "Comunicado 6",
        "nombre_archivo": "R, V, Z-V",
        "descripcion": "oooooooooo o   oooooooo oooooo oooo o o o",
        "imagen1": "../../../../assets/img/comunicados/R, V, Z-V.jpg",
        "tipo1": ".jpeg",
        "imagen2": "undefined",
        "tipo2": "undefined"
    },
    {
        "id_addComunicado": 11,
        "titulo": "La horraca",
        "nombre_archivo": "LIBROS_yiaaaa (7)",
        "descripcion": "aaaaaaaaaaaaaaaaaaaa",
        "imagen1": "../../../../assets/img/comunicados/LIBROS_yiaaaa (7).pdf",
        "tipo1": ".pdf",
        "imagen2": "undefined",
        "tipo2": "undefined"
    },
    {
        "id_addComunicado": 12,
        "titulo": "ccccccccccc",
        "nombre_archivo": "Captura de pantalla 2022-08-18 073935",
        "descripcion": "ccccccccccc",
        "imagen1": "../../../../assets/img/comunicados/Captura de pantalla 2022-08-18 073935.jpeg",
        "tipo1": ".jpeg",
        "imagen2": "undefined",
        "tipo2": "undefined"
    },
    {
        "id_addComunicado": 13,
        "titulo": "One Piece",
        "nombre_archivo": "Your Electronic Ticket Receipt",
        "descripcion": "aaaaaaaaaaaa",
        "imagen1": "../../../../assets/img/comunicados/Your Electronic Ticket Receipt.pdf",
        "tipo1": ".pdf",
        "imagen2": "undefined",
        "tipo2": "undefined"
    }
];

  constructor(private http: HttpClient) {
  }


  ngOnInit(): void {
  }

  

}

interface Item {
  imageSrc: string;
  imageAlt: string
}

export interface comunicadoI{ 
  id_addComunicado: number;
  titulo: string,
  nombre_archivo: string,
  descripcion: string,
    imagen1: string,
    tipo1: string,
    imagen2?: string,
    tipo2?: string,
}

// propiedades: any = {
//   error: true
// }
// @Input() galleryData: Item[] = []
// // @ViewChild('asImage') image!: ElementRef;

// apiUrl = 'http://localhost:3000/addcomunicado';
// readData!: comunicadoI[];

// constructor(private http: HttpClient) { 
//   this.getInfo();
// }


// ngOnInit(): void {
// }


// // get all data
// getAllData(): Observable<any>{
//   return this.http.get(`${this.apiUrl}`);
// }

// getInfo(){
//   this.getAllData().subscribe((res) => {
//     this.readData = res.data;
//     console.log('sda', this.readData, 'adva');
//   })
// }
