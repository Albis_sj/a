import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { ComponentsComponent } from './components.component';
import { EditorialesComponent } from './editoriales/editoriales.component';
import { InicioComponent } from './inicio/inicio.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { AsociadosComponent } from './asociados/asociados.component';
import { ComunicadosComponent } from './comunicados/comunicados.component';
import { ContactoComponent } from './contacto/contacto.component';
import { LibrosComponent } from './libros/libros.component';
import { EventosComponent } from './eventos/eventos.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { PipesModule } from '../pipes/pipes.module';
import { UnLibroComponent } from './un-libro/un-libro.component';


@NgModule({
  declarations: [
    ComponentsComponent,
    EditorialesComponent,
    InicioComponent,
    NosotrosComponent,
    AsociadosComponent,
    ComunicadosComponent,
    ContactoComponent,
    LibrosComponent,
    EventosComponent,
    UnLibroComponent,
  ],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedModule,
    NgxPaginationModule,
    PipesModule
  ],
  exports: [
    EditorialesComponent,
    InicioComponent,
    NosotrosComponent,
    AsociadosComponent,
    ComunicadosComponent,
    ContactoComponent,
    LibrosComponent,
    EventosComponent,
  ]
})
export class ComponentsModule { }
