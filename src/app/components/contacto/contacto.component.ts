import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  formulario!: FormGroup;
  mensaje!:string;
  enviado: string = 'El mensaje fue enviado con éxito'

  propiedades: any = {
    error: false
  }

  constructor(private fb : FormBuilder) {
    this.crearFormulario();
   }

   crearFormulario(): void {
    this.formulario = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      mensaje: ['', [Validators.required, Validators.minLength(7)]],
    });
  }

  
  //=========================================
  //=================== GET =================
  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }
  
  get emailNoValido(){
    return this.formulario.get('email')?.invalid && this.formulario.get('email')?.touched
  }

  get mensajeNoValido(){
    return this.formulario.get('mensaje')?.invalid && this.formulario.get('mensaje')?.touched
  }
  //=========================================

  ngOnInit(): void {
  }

  guardar():void{
    this.mensaje = this.enviado;
    this.limpiarFormulario();

    this.mensaje = this.enviado;
    setTimeout(() => {
      this.mensaje = '';
    }, 14000);

  this.propiedades.error = true;
  setTimeout(() => {
    this.propiedades.error =false;
  }, 14000);
  }

  limpiarFormulario(){
    this.formulario.reset()
  }

}
