
import { Component, Inject, OnInit } from '@angular/core';
// import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoriaService } from 'src/app/servicios/categoria.service';
import { Usuario } from 'src/app/models/usuario.model';
import { LibroService } from 'src/app/servicios/libro.service';
import { NewImageService } from '../../servicios/new-image.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-modal-imagen',
  templateUrl: './modal-imagen.component.html',
  styleUrls: ['./modal-imagen.component.css']
})
export class ModalImagenComponent implements OnInit {
  usuario!: Usuario;

  public imagenSubir!: File;
  public imgTemp2: any = null;
  public habilitar: boolean = true;
  seleccionado!: string | ArrayBuffer | null;

  constructor(
    // date: DateAdapter<Date>,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private dialogRef: MatDialogRef<ModalImagenComponent>,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any,
    private categoriaService: CategoriaService,
    private libroService: LibroService,
    private nuevaImagenService: NewImageService
  ) {
    // date.getFirstDayOfWeek = () => 1;
    this.usuario = this.usuarioService.usuario;
    console.log(this.usuario);
    console.log(this.datoEdicion);
    
  }

  ngOnInit(): void {
  }

  subirImagen(){
    this.nuevaImagenService.actualizarFoto(
      this.imagenSubir, 
      'libros',
      this.datoEdicion._id,
      ).then(img => {
        this.habilitar = true;
        this.dialogRef.close('save');
      }).catch(err => {
        Swal.fire('Ocurrio un Error', err.error.msg, 'error');
      });
   }

  cambiarImagen(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.imagenSubir = <File>event.target.files[0];
      console.log(event.target.files[0]);
      // this.tipo_archivo = file.type;
        const reader = new FileReader();
        reader.onload = (event: any) => 
        // this.seleccionado = reader.result;
        this.imgTemp2 = reader.result;
        reader.readAsDataURL(this.imagenSubir);
        this.habilitar = false;
    } else {
      this.imgTemp2 = null;
    }
  }
}

// EditarCategoria() {
  //   if (!this.datoEdicion) {
  //     this.formSubmited = true;
  //     if (this.categoriaForm.invalid) {
  //       return;
  //     }
  //     this.categoriaService.crearCategoria(this.categoriaForm.value).subscribe({
  //       next: (res) => {
  //         this.categoriaForm.reset();
  //         this.dialogRef.close('save');
  //       },
  //       error: (error) => {
  //         alert('Error al Crear Categoria');
  //       },
  //     });
  //   } else {
  //     if (this.categoriaForm.invalid) {
  //       return;
  //     }  
  //     this.ActualizarCategoria();
  //   }
  // }

