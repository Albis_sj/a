import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { SliderComponent } from './slider/slider.component';
import { ModalImagenComponent } from './modal-imagen/modal-imagen.component';
import { MaterialModule } from '../admin/shared/material.module';
import { PipesModule } from '../pipes/pipes.module';



@NgModule({
  declarations: [
    NavbarComponent,
    FooterComponent,
    SliderComponent,
    ModalImagenComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    PipesModule
  ],
  exports:[
    NavbarComponent,
    FooterComponent,
    SliderComponent
  ] 
})
export class SharedModule { }
