import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario.model';
import { UsuarioService } from 'src/app/servicios/usuario.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public autenticado: boolean = true;

  public usuario!: Usuario;
  propiedades: any = {
    visible: false
  }

  constructor(
    private router: Router,
    private usuarioService: UsuarioService) {
  }

  ngOnInit(): void {
    this.autenticado = this.usuarioService.token === '' ? true : false;
    this.usuario = this.usuarioService.usuario;
  }

  buscarLibro(termino: string): void{
    console.log(termino);  
    this.router.navigate(['/libros', termino]);
  }

  navegar(){
    this.router.navigate(['/perfil']);
  }

}
