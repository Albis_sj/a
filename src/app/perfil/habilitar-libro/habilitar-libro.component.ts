import { Component, Inject, OnInit } from '@angular/core';
// import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoriaService } from 'src/app/servicios/categoria.service';
import { Usuario } from 'src/app/models/usuario.model';
import { LibroService } from 'src/app/servicios/libro.service';


@Component({
  selector: 'app-habilitar-libro',
  templateUrl: './habilitar-libro.component.html',
  styleUrls: ['./habilitar-libro.component.css']
})

export class HabilitarLibroComponent implements OnInit {
  usuario!: Usuario;
  habilitar: boolean = true;
  mensaje: string = '';

  constructor(
    // date: DateAdapter<Date>,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private dialogRef: MatDialogRef<HabilitarLibroComponent>,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any,
    private categoriaService: CategoriaService,
    private libroService: LibroService,
  ) {
    // date.getFirstDayOfWeek = () => 1;
    this.usuario = this.usuarioService.usuario;
    console.log(this.usuario);
    console.log(this.datoEdicion);
    
  }

  ngOnInit(): void {
    this.verificarCuenta();
  }

  verificarCuenta(){
    if (this.usuario.coin === 0) {
      this.mensaje = 'No puedes Habilitar este Libro por Ahora'
    } else {
      let numero = this.usuario.coin / 10;
      this.mensaje = `Actualmente Puedes Habilitar ${numero} Libro(s)`
      this.habilitar = false;
    }
  }

  ActualizarLibro() {
    if (this.datoEdicion.habilitado) {
      return;
    } else if (!this.habilitar){
      this.libroService.habilitarLibro(this.datoEdicion._id)
        .subscribe({
          next: (resp: any) => {
            this.usuario.coin = resp.coin;
            this.dialogRef.close('save');
          },
          error: (error) => {
            console.log(error);
          },
          complete: () => {
            console.log('COMPLETADO');
          }
        })
    }
  }
}

// EditarCategoria() {
  //   if (!this.datoEdicion) {
  //     this.formSubmited = true;
  //     if (this.categoriaForm.invalid) {
  //       return;
  //     }
  //     this.categoriaService.crearCategoria(this.categoriaForm.value).subscribe({
  //       next: (res) => {
  //         this.categoriaForm.reset();
  //         this.dialogRef.close('save');
  //       },
  //       error: (error) => {
  //         alert('Error al Crear Categoria');
  //       },
  //     });
  //   } else {
  //     if (this.categoriaForm.invalid) {
  //       return;
  //     }  
  //     this.ActualizarCategoria();
  //   }
  // }
