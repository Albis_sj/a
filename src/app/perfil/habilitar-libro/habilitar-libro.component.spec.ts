import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HabilitarLibroComponent } from './habilitar-libro.component';

describe('HabilitarLibroComponent', () => {
  let component: HabilitarLibroComponent;
  let fixture: ComponentFixture<HabilitarLibroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HabilitarLibroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HabilitarLibroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
