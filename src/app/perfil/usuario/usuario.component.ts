import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  Inject,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Usuario } from 'src/app/models/usuario.model';
import { LibroService } from 'src/app/servicios/libro.service';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { Categoria } from '../../models/categoria.model';

import Swal from 'sweetalert2';
import { Libro } from 'src/app/models/libro.model';

import { DateAdapter } from '@angular/material/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
})
export class UsuarioComponent implements OnInit {
  public convertir(fecha: any) {}
  public perfilForm!: FormGroup;
  public misLibros: Libro[] = [];
  public categoriaSeleccionada!: Categoria;
  public libroSeleccionado!: Libro;
  public formLibro!: FormGroup;
  public usuario: Usuario;

  constructor(
    date: DateAdapter<Date>,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private dialogRef: MatDialogRef<UsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any
  ) {
    date.getFirstDayOfWeek = () => 1;
    this.usuario = this.usuarioService.usuario;
  }

  ngOnInit(): void {
    this.perfilForm = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern(
            /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/
          ),
        ],
      ],
      celular: ['', [Validators.minLength(8), Validators.maxLength(8)]],
      celular2: ['', [Validators.minLength(8), Validators.maxLength(8)]],
      telefono: ['', [Validators.minLength(5), Validators.maxLength(8)]],
      telefono2: ['', [Validators.minLength(5), Validators.maxLength(8)]],
      direccion: ['', Validators.minLength(10)],
      page_web: ['', Validators.minLength(5)],
    });

    if (this.datoEdicion) {
      console.log('PERFIL ACTUAL', this.datoEdicion);
      
      this.mensajeBoton = 'Guardar Cambios';
      this.perfilForm.patchValue(this.datoEdicion);
      console.log('PERFIL Cambiado', this.perfilForm.value);

    }
  }

  tipo_archivo: any;

  // IMAGEN
  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef;

  tabla: boolean = true;
  file!: File;

  cerrarSession() {
    this.usuarioService.logout();
  }

  // public usuariosForm!: FormGroup;
  hide = true;
  formSubmited: boolean = false;
  usuarios = ['Hola', 'Hola1', 'Hola 3'];
  mensajeBoton: string = 'Crear Usuario';

  EditarUsuario() {
    if (!this.datoEdicion) {
      this.formSubmited = true;
      if (this.perfilForm.invalid) {
        return;
      }
      // this.usuarioService.cambiarUsuario();
      this.perfilForm.reset();
      this.dialogRef.close('save');
      console.log(this.perfilForm.value);
    } else {
      this.ActualizarUsuario();
    }
  }

  ActualizarUsuario() {
    if (!this.perfilForm.valid) {
      console.log('No es valido');
      return;
    }
    this.usuarioService
      .actualizarPerfil(this.perfilForm.value)
      .subscribe({
        next: (res) => {
          //TODO Aca se actualiza automaticamente porque los objetod en javascript son pasados por referencia
          const {nombre, email, celular, celular2, telefono, telefono2, direccion, page_web } = this.perfilForm.value;
          this.usuario.nombre = nombre;
          this.usuario.email = email;
          this.usuario.celular = celular;
          this.usuario.celular2 = celular2;
          this.usuario.telefono = telefono;
          this.usuario.telefono2 = telefono2;
          this.usuario.direccion = direccion;
          this.usuario.page_web = page_web;
          Swal.fire('Guardado', 'Cambios fueron guardados', 'success');
          this.perfilForm.reset();
          this.dialogRef.close('update');
        },
        error: (err) => {
          console.log(err);
          alert('Error al Actualizar');
        },
      });
  }

  // campoNoValido(campo: string): boolean {
  //   if (this.usuariosForm.get(campo)?.invalid && this.formSubmited) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // }
}

// actualizarPerfil(){
//   this.usuarioService.actualizarPerfil(this.perfilForm.value)
//   .subscribe(
//         {
//           next: (resp) => {
//             //TODO Aca se actualiza automaticamente porque los objetod en javascript son pasados por referencia
//             const {nombre, celular, celular2, telefono, telefono2, direccion, page_web} = this.perfilForm.value;
//             this.usuario.nombre = nombre;
//             this.usuario.celular = celular;
//             this.usuario.celular2 = celular2;
//             this.usuario.telefono = telefono;
//             this.usuario.telefono2 = telefono2;
//             this.usuario.direccion = direccion;
//             this.usuario.page_web = page_web;

//             Swal.fire('Guardado', 'Tus Cambios Fueron Guardados', 'success');
//           },
//           error: (err) => {
//             Swal.fire('Ocurrio un Error', err.error.msg, 'error');
//           },
//           complete: () => {
//             console.log('Completado Correctamente');
//           }
//         });
// }
