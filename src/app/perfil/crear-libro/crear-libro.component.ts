import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { materialize } from 'rxjs';
import { LibroService } from 'src/app/servicios/libro.service';
import { UsuarioService } from 'src/app/servicios/usuario.service';

@Component({
  selector: 'app-crear-libro',
  templateUrl: './crear-libro.component.html',
  styleUrls: ['./crear-libro.component.css'],
})
export class CrearLibroComponent implements OnInit {
  idioma: string[] = ['Español', 'Inglés'];
  estado: string[] = ['Nuevo', 'Muy bueno', 'Bueno', 'Sin especificar'];
  edad: string[] = [
    'Niños (0-5 años)',
    'Niños (5-10)',
    'Jovenes (12-17)',
    'Adolescentes (17-18)',
    '(20- 28)',
    'Mayores',
    'Todo público',
  ];
  formato: string[] = [
    'Libro de bolsillo',
    'Libro en rústica',
    'Tapa Dura',
    'Anillado',
    'Encuadernado con grapas',
    'Encuadernado en cuero',
    'Encuadernado flexi',
    'Folleto, panfleto',
    'Libro de cartón',
    'Libro de goma espuma',
    'Libro de tela',
    'Libro electrónico',
    'Con mapa plegable',
    'Sin especificar',
  ];
  formulario!: FormGroup;
  Form = {};
  tipo_archivo: any;

  // IMAGEN
  url =
    'https://i.pinimg.com/736x/b5/49/41/b5494197b2d462c940f88988b203d290.jpg';
  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef;
  apiUrlImg = 'http://localhost:3000/addLibroImg';

  enviado: string = 'Registro finalizado con éxito';
  mensaje!: string;

  file!: File;
  usuario!: any;
  Snack: any = {
    error: false,
  };
  propiedades: any = {
    error: false,
  };
  userName!: number;
  mostrar = false;
  adicionar: boolean = true;

  constructor(
    private _librosSVC: LibroService,
    private _vendedorSVC: UsuarioService,
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient,
    private activateRouter: ActivatedRoute
  ) {
    // this.crearFormulario();
    this.getCategoria();

    this.activateRouter.params.subscribe((params) => {
      const id = params['id'];
      console.log(id, '000');

      this.formulario = this.fb.group({
        precio: ['', Validators.required],
        // hora_publicacion:[''],
        estado: [''],
        id_categoria: ['', Validators.required],
        formato: [''],
        // ubicacion: [''],
        idioma: ['', Validators.required],
        autor_nombre: ['', [Validators.required, Validators.minLength(3)]],
        autor_apellido: ['', Validators.minLength(3)],
        anio_publicacion: [''],
        edad_publico: [''],
        numero_paginas: [''],

        titulo: ['', [Validators.required, Validators.minLength(3)]],
        subtitulo: ['', [Validators.minLength(3)]],
        descripcion: ['', [Validators.required, Validators.minLength(3)]],

        imagen: ['', Validators.required],

        // pago:  [''],
        // fecha_lim ite:  [''],
        // id_vendedor:  [''],
      });

      if (id !== 'nuevo') {
        // console.log('distinto de nuevo');

        this.getUnLibro(id);
        this.adicionar = false;

        setTimeout(() => {
          console.log('4000');
          console.log(this.usuario, 'usu');

          //Verifico que la longitud del objeto es cero
          if (Object.keys(this.usuario).length === 0) {
            this.router.navigate(['/perfil']);
          }

          this.formulario.patchValue({
            idioma: this.usuario.idioma,
            imagen1: this.usuario.imagen1 + this.usuario.tipo1,
            tipo1: this.usuario.tipo1,
            precio: this.usuario.precio,
            estado: this.usuario.estado,
            id_categoria: this.usuario.id_categoria,
            formato: this.usuario.formato,
            autor_nombre: this.usuario.autor_nombre,
            autor_apellido: this.usuario.autor_apellido,
            anio_publicacion: this.usuario.anio_publicacion,
            edad_publico: this.usuario.edad_publico,
            numero_paginas: this.usuario.numero_paginas,

            titulo: this.usuario.titulo,
            subtitulo: this.usuario.subtitulo,
            descripcion: this.usuario.descripcion,
          });

          this.url = this.usuario.imagen1 + this.usuario.tipo1;
        }, 2000);
      } else {
        console.log('else');
      }
    });

    // recuperar el id del usuario
    let nombre = sessionStorage.getItem('ID');
    if (nombre === null) {
      console.log('null');

      this.userName = 0;
    } else {
      this.userName = parseInt(nombre);
      // console.log(this.userName, 'nombre');

      this.mostrar = true;

      this.getPerfil();
      // this.getAll();
      // console.log('getall');
    }
  }

  // VALIDACIONES
  get precioNoValido() {
    return (
      this.formulario.get('precio')?.invalid &&
      this.formulario.get('precio')?.touched
    );
  }
  get id_categoriaNoValido() {
    return (
      this.formulario.get('id_categoria')?.invalid &&
      this.formulario.get('id_categoria')?.touched
    );
  }
  get idiomaNoValido() {
    return (
      this.formulario.get('idioma')?.invalid &&
      this.formulario.get('idioma')?.touched
    );
  }
  get autor_nombreNoValido() {
    return (
      this.formulario.get('autor_nombre')?.invalid &&
      this.formulario.get('autor_nombre')?.touched
    );
  }
  get imagenNoValido() {
    return (
      this.formulario.get('imagen')?.invalid &&
      this.formulario.get('imagen')?.touched
    );
  }
  get tituloNoValido() {
    return (
      this.formulario.get('titulo')?.invalid &&
      this.formulario.get('titulo')?.touched
    );
  }
  get descripcionNoValido() {
    return (
      this.formulario.get('descripcion')?.invalid &&
      this.formulario.get('descripcion')?.touched
    );
  }
  get formatoNoValido() {
    return (
      this.formulario.get('formato')?.invalid &&
      this.formulario.get('formato')?.touched
    );
  }

  crearFormulario() {
    this.formulario = this.fb.group({
      precio: ['', Validators.required],
      // hora_publicacion:[''],
      estado: ['Sin Especificar'],
      id_categoria: ['', Validators.required],
      formato: [''],
      // ubicacion: [''],
      idioma: ['', Validators.required],
      autor_nombre: ['', [Validators.required, Validators.minLength(3)]],
      autor_apellido: ['', Validators.minLength(3)],
      anio_publicacion: [''],
      edad_publico: [''],
      numero_paginas: [''],

      titulo: ['', [Validators.required, Validators.minLength(3)]],
      subtitulo: ['', [Validators.minLength(3)]],
      descripcion: ['', [Validators.required, Validators.minLength(3)]],

      imagen: ['', Validators.required],

      // pago:  [''],
      // fecha_lim ite:  [''],
      // id_vendedor:  [''],
    });
  }

  readData: any;
  tabla: boolean = true;
  readCategoria: any;
  readPerfil: any = {
    nombre: '',
    id_vendedor: 0,
    categoria: '',
    correo1: '',
    correo2: 0,
    contrasenia: '',
    telefono1: '',
    telefono2: '',
    celular1: 0,
    celular2: '',
    direccion: '',
    pagina: '',
  };

  getCategoria() {
    // this._librosSVC.getAllDataCate().subscribe((res)=>{
    //   this.readCategoria = res.data;
    // });
  }
  getPerfil() {
    // this._vendedorSVC.getOneData(this.userName).subscribe((res)=>{
    //   this.readPerfil = res.data[0];
    // });
  }

  getUnLibro(id: number) {
    // console.log(id, 'string o number');
    // // const gid = parseInt(id);
    // this._librosSVC.getUnLibro(id).subscribe((res)=>{
    //   console.log(res, 'res libro Update');
    //   this.usuario = res.data[0]
    //   console.log(this.usuario, 'usuazzzz');
    //   // this.router.navigate([ `perfil`])
    // });
  }

  ngOnInit(): void {}

  guardar() {
    // console.log('guardar');

    if (this.adicionar) {
      console.log('adicionar');

      let nombreImagen = this.formulario.value.imagen;
      const imgName = nombreImagen.split('\\');
      const dividir = imgName[2].split('.');
      const extension = this.tipo_archivo.split('/');

      if (this.formulario.valid) {
        // console.log(this.formulario.value);
        let hora = new Date();
        this.Form = {
          precio: this.formulario.value.precio,
          hora_publicacion: hora,
          estado: this.formulario.value.estado,
          id_categoria: this.formulario.value.id_categoria,
          formato: this.formulario.value.formato,
          ubicacion: 'Cochabamba, borrar este campo',
          idioma: this.formulario.value.idioma,
          autor_nombre: this.formulario.value.autor_nombre,
          autor_apellido: this.formulario.value.autor_apellido,
          anio_publicacion: this.formulario.value.anio_publicacion,
          edad_publico: this.formulario.value.edad_publico,
          numero_paginas: this.formulario.value.numero_paginas,

          titulo: this.formulario.value.titulo,
          subtitulo: this.formulario.value.subtitulo,
          descripcion: this.formulario.value.descripcion,

          imagen1: `../../../../assets/img/libros/${dividir[0]}.${dividir[1]}`,
          tipo1: `.${extension[1]}`,

          pago: 'false',
          fecha_limite: '',
          id_vendedor: this.userName,
        };
        console.log(this.Form);

        this.onFileUpload();

        this.mensaje = this.enviado;
        this.limpiarFormulario();
        setTimeout(() => {
          this.mensaje = '';
        }, 4000);

        this.Snack.error = true;
        setTimeout(() => {
          this.Snack.error = false;
        }, 4000);
        this.propiedades.error = false;
        this.createData(this.Form);
        // this.router.navigate([ `perfil`])
      }
    } else {
      console.log('modificar usuario');

      this.Form = {
        precio: this.formulario.value.precio,
        hora_publicacion: this.usuario.hora_publicacion,
        estado: this.formulario.value.estado,
        id_categoria: this.formulario.value.id_categoria,
        formato: this.formulario.value.formato,
        ubicacion: 'Cochabamba, borrar este campo',
        idioma: this.formulario.value.idioma,
        autor_nombre: this.formulario.value.autor_nombre,
        autor_apellido: this.formulario.value.autor_apellido,
        anio_publicacion: this.formulario.value.anio_publicacion,
        edad_publico: this.formulario.value.edad_publico,
        numero_paginas: this.formulario.value.numero_paginas,

        titulo: this.formulario.value.titulo,
        subtitulo: this.formulario.value.subtitulo,
        descripcion: this.formulario.value.descripcion,

        imagen1: this.usuario.imagen1,
        tipo1: this.usuario.tipo1,

        pago: this.usuario.pago,
        fecha_limite: this.usuario.fecha_limite,
        id_vendedor: this.usuario.id_vendedor,
      };

      this.updateData(this.Form, this.usuario.id_libro);

      console.log('MODIFICAR', this.Form, 'MODIFICAR');
    }
  }

  actualizarLibro() {}

  limpiarFormulario() {
    this.formulario.reset();
  }

  // On File Select
  onSelect(event: any) {
    this.file = event.target.files[0];
    console.log(this.file.type);
    const filew = event.target.files[0];
    this.tipo_archivo = filew.type;

    if (event.target.files[0]) {
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      };
    }
  }

  onFileUpload() {
    // console.log('upload foto');
    // console.log('FILE INOUT', this.fileInput.nativeElement.files[0], 'FILE INPUT');
    const imageBlob = this.fileInput.nativeElement.files[0];
    const file = new FormData();
    // console.log('prim', file, 'primero');

    file.set('file', imageBlob);

    this.http.post(`${this.apiUrlImg}`, file).subscribe((res) => {
      // console.log('segundo', file, 'CUARTO');
    });
  }

  //create data
  createData(data: any) {
    // console.log(data);
    // this._librosSVC.createData(data).subscribe((res)=>{
    //   console.log(res, 'res create REgistro==>');
    //   // this.getAll();
    //   this.router.navigate([ `perfil`])
    // });
  }

  updateData(data: any, id: number) {
    // console.log(data);
    // this._librosSVC.updateData(data, id).subscribe((res)=>{
    //   console.log('update');
    // });
  }
}

export interface FormLibroI {
  idioma: string;
  titulo: string;
  subtitulo: string;
  autor_nombre: string;
  autor_apellido: string;
  descripcion: string;
  precio: number;
  anio_publicacion: number;
  formato: string;
  id_categoria: number;
  imagen1: string;
  tipo1: string;
  estado: string;
  edad_publico: string;
  numero_paginas: number;
  // id_libro: number,
  // hora_publicacion: string,
  // ubicacion: string,

  // pago: string,
  // fecha_limite: string,
  // id_vendedor: number,
}
