import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { UsuarioComponent } from './usuario/usuario.component';
import { PerfilComponent } from './perfil.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CrearLibroComponent } from './crear-libro/crear-libro.component';
import { MaterialModule } from '../admin/shared/material.module';
import { ModalLibroComponent } from './modal-libro/modal-libro.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { HabilitarLibroComponent } from './habilitar-libro/habilitar-libro.component';
import { PipesModule } from '../pipes/pipes.module';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    UsuarioComponent,
    PerfilComponent,
    CrearLibroComponent,
    ModalLibroComponent,
    HabilitarLibroComponent
  ],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxPaginationModule,
    PipesModule,
    SharedModule
  ],
  exports: [
    UsuarioComponent
  ]
})
export class PerfilModule { }
