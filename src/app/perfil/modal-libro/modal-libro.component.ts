import {
  Component,
  Inject,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/servicios/usuario.service';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoriaService } from 'src/app/servicios/categoria.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LibroService } from 'src/app/servicios/libro.service';
import { Usuario } from 'src/app/models/usuario.model';
import { Categoria } from 'src/app/models/categoria.model';
import Swal from 'sweetalert2';
import { NewImageService } from 'src/app/servicios/new-image.service';
import { Subscription } from 'rxjs';

interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}

@Component({
  selector: 'app-modal-libro',
  templateUrl: './modal-libro.component.html',
  styleUrls: ['./modal-libro.component.css'],
})
export class ModalLibroComponent implements OnInit {
  leer: boolean = false;
  usuario!: Usuario;
  readCategoria: Categoria[] = [];
  idioma: string[] = ['Español', 'Inglés'];
  estado: string[] = ['Nuevo', 'Bueno', 'Muy bueno'];
  edad: string[] = [
    'Niños (0-5 años)',
    'Niños (5-10 años)',
    'Jovenes (12-17 años)',
    'Adolescentes (17-18 años)',
    '(20-28 años)',
    'Mayores',
    'Todo público',
  ];
  formato: string[] = [
    'Tapa Dura',
    'Con mapa plegable',
    'Anillado',
    'Encuadernado flexi',
    'Encuadernado en cuero',
    'Encuadernado con grapas',
    'Folleto, panfleto',
    'Libro de cartón',
    'Libro de tela',
    'Libro en rústica',
    'Libro de bolsillo',
    'Libro electrónico',
    'Libro de goma espuma',
  ];

  formLibro!: FormGroup;
  // tipo_archivo: any;
  seleccionado!: string | ArrayBuffer | null;
  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef;
  readData: any;
  tabla: boolean = true;
  file!: File;
  // public categoriaForm!: FormGroup;
  hide = true;
  formSubmited: boolean = false;
  mensajeBoton: string = 'Crear Libro';

  ocultarImagen: boolean = true;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activateRouter: ActivatedRoute,
    date: DateAdapter<Date>,
    private usuarioService: UsuarioService,
    private dialogRef: MatDialogRef<ModalLibroComponent>,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any,
    private categoriaService: CategoriaService,
    private libroService: LibroService,
    private nuevaImagenService: NewImageService
  ) {
    this.usuario = this.usuarioService.usuario;
    date.getFirstDayOfWeek = () => 1;
    // this.activateRouter.params.subscribe((params) => {
    //   const id = params['id'];
    //   console.log('Escuchando La Ruta Activa');
    // });
  }
  ngOnInit(): void {
    this.getCategoria();
    this.crearFormulario();
    if (this.datoEdicion) {
      this.mensajeBoton = 'Guardar Cambios';
      this.formLibro.patchValue(this.datoEdicion);
      console.log(this.formLibro.value); 
      this.formLibro.patchValue({ categoria: this.datoEdicion.categoria._id });
      if (this.formLibro.get('habilitado')?.value) {
        this.leer = true;
      }
      this.ocultarImagen = false;

    }
  }

  crearFormulario() {
    this.formLibro = this.fb.group({
      _id: [''],
      titulo: ['', [Validators.required, Validators.minLength(3)]],
      descripcion: ['', [Validators.required, Validators.minLength(3)]],
      autor: ['', [Validators.required, Validators.minLength(3)]],
      precio: ['', Validators.required],
      formato: [''],
      idioma: ['', Validators.required],
      categoria: ['', Validators.required],
      habilitado: [''],
      date_publicacion: [''],
      subtitulo: ['', [Validators.minLength(3)]],
      estado: [''],
      edad_publico: [''],
      n_paginas: [''],
      img: [''],
    });
  }

  onSelect(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.file = <File>event.target.files[0];
      console.log(event.target.files[0]);

      // this.tipo_archivo = file.type;
      const reader = new FileReader();
      reader.onload = (event: any) => (this.seleccionado = reader.result);
      reader.readAsDataURL(this.file);
      console.log(this.file);
    }
  }

  getCategoria() {
    this.categoriaService
      .cargarCategorias2()
      .subscribe((categorias: Categoria[]) => {
        this.readCategoria = categorias;
      });
  }

  CrearLibro() {
    this.formSubmited = true;
    if (!this.datoEdicion) {
      if (this.formLibro.invalid) {
        return;
      }
      if (!this.file) {
        Swal.fire(
          'Verifique sus datos',
          `Por Favor Llene los campos requeridos`,
          'question'
        );
        return;
      }
      this.libroService.crearLibro(this.formLibro.value, this.file).subscribe({
        next: (res: any) => {
          const id = res.libro._id.toString()
          if (id && this.file) {
            this.nuevaImagenService
              .actualizarFoto(this.file, 'libros', id)
              .then((img) => {
                Swal.fire(
                  'Creación de Libro',
                  `El Libro fue Creado Correctamente`,
                  'success'
                );
                this.formLibro.reset();
                this.dialogRef.close('save');
              })
              .catch((err) => {
                Swal.fire('Ocurrio un Error', err.error.msg, 'error');
              });
          }
        },
        error: (error) => {
          console.log(error);
        },
      });
    } else {
      if (this.formLibro.invalid) {
        return;
      }
      this.ActualizarLibro();
    }
  }

  ActualizarLibro() {
    this.libroService.actualizarLibro(this.formLibro.value).subscribe({
      next: (res) => {
        Swal.fire(
          'Actualización de Libro',
          `El Libro fue Actualizado Correctamente`,
          'success'
        );
        this.formLibro.reset();
        this.dialogRef.close('update');
      },
      error: (error) => {
        alert(error + 'Ocurrio un error');
      },
    });
  }
}

// onFileUpload() {
//   // console.log('FILE INOUT', this.fileInput.nativeElement.files[0], 'FILE INPUT');
//   const imageBlob = this.fileInput.nativeElement.files[0];
//   const file = new FormData();
//   // console.log('prim', file, 'primero');
//   file.set('file', imageBlob);
//   this.http.post(`${this.apiUrlImg}`, file).subscribe((res) => {
//     // console.log('segundo', file, 'CUARTO');
//   });
// }

// campoNoValido(campo: string): boolean {
//     if ((this.categoriaForm.get(campo)?.invalid && this.formSubmited) || (this.formLibro.get(campo)?.invalid && this.formLibro.get(campo)?.touched)) {
//       return true;
//     } else {
//       return false;
//     }
//   }

//   guardar() {
//     let nombreImagen = this.formLibro.value.;
//     const imgName = nombreImagen.split('\\');
//     const dividir = imgName[2].split('.');
//     const extension = this.tipo_archivo.split('/');
//     this.onFileUpload();
//   }
