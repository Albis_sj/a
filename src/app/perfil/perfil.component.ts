import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario.model';
import { LibroService } from 'src/app/servicios/libro.service';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { Categoria } from '../models/categoria.model';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { Libro } from 'src/app/models/libro.model';
import { ModalLibroComponent } from './modal-libro/modal-libro.component';
import { ViewEncapsulation } from '@angular/core';
import { UsuarioComponent } from './usuario/usuario.component';
import { HabilitarLibroComponent } from './habilitar-libro/habilitar-libro.component';

import { NewImageService } from '../servicios/new-image.service'
import { ModalImagenComponent } from '../shared/modal-imagen/modal-imagen.component';
import { delay } from 'rxjs';
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PerfilComponent implements OnInit {
  pageActual: number = 1;
  readData: Libro[] = [];
  public convertir(fecha: any) { }
  public misLibros: Libro[] = [];
  public categoriaSeleccionada!: Categoria;
  public libroSeleccionado!: Libro;
  public usuario: Usuario;
  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef;
  newEnviar: boolean = true;
  ReadOnly: boolean = true;
  file!: File;
  public perfilForm!: FormGroup;
  public imagePerfil!: File;
  public imgTemp: any = null;


  constructor(
    public usuarioService: UsuarioService,
    private libroService: LibroService,
    private dialog: MatDialog,
    private nuevaImagenService: NewImageService,
  ) {
    this.usuario = this.usuarioService.usuario;
  }

  ngOnInit(): void {
    console.log(this.usuario);
    
    this.cargarLibros();
  }
  



  actualizarPerfil(){
    this.usuarioService.actualizarPerfil(this.perfilForm.value)
    .subscribe(
          {
            next: (resp) => {
              //TODO Aca se actualiza automaticamente porque los objetod en javascript son pasados por referencia
              const {nombre, email } = this.perfilForm.value;
              this.usuario.nombre = nombre;
              this.usuario.email = email;

              Swal.fire('Guardado', 'Cambios fueron guardados', 'success');
            },
            error: (err) => {
              Swal.fire('Ocurrio un Error', err.error.msg, 'error');
            },
            complete: () => {
              console.log('Completado Papu :V Perfil');
            }
          });
  }

  cambiarImagen(event: any){
    if(event.target.files[0]){
        this.imagePerfil = event.target.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onloadend = () => {
          this.imgTemp = reader.result;
          this.newEnviar = false;
        }
      } else {
        this.imgTemp = null;
      }
  }

  subirImagen(){
    this.nuevaImagenService.actualizarFoto(
      this.imagePerfil, 
      'usuarios',
      this.usuario.uid!,
      ).then(img => {
        this.usuario.img = img;
        this.newEnviar = true;
      }).catch(err => {
        Swal.fire('Ocurrio un Error', err.error.msg, 'error');
      });
   }

  cargarLibros() {
    this.libroService.cargarMisLibros().subscribe({
      next: (resp) => {
        console.log(resp);
        this.readData = resp;
      },
      error: (err) => {
        console.log(err.errors.msg);
      }
    })
  }
  cerrarSession() {
    this.usuarioService.logout();
  }

  openDialog() {
    if (screen.width < 500) {
      this.dialog
        .open(ModalLibroComponent, {
          // maxWidth: '100vW',
          // width: '80vW',
          // // height: '80vW',
          // panelClass: 'custom-modalbox'
        })
        .afterClosed().pipe(
          delay(500)
        ).subscribe((valor) => {
          if (valor === 'save') {
            this.cargarLibros();
          }
        });
    } else {
      this.dialog
        .open(ModalLibroComponent, {
          // width: '60%',
          // height: '100%',

        })
        .afterClosed().pipe(
          delay(500)
        )
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarLibros();
          }
        });
    }
  }


  editarLibro(valor: Libro) {
    if (screen.width < 500) {
      this.dialog
        .open(ModalLibroComponent, {
          //   maxWidth: '100vW',
          //   width: '80vW',
          //   height: '80vW',
          // panelClass: 'custom-modalbox'
          data: valor

        })
        .afterClosed().pipe(
          delay(500)
        )
        .subscribe((valor) => {
          if (valor === 'update') {
            this.cargarLibros();
          }
        });
    } else {
      this.dialog
        .open(ModalLibroComponent, {
          width: '60%',
          // height: '100%'
          data: valor
        })
        .afterClosed().pipe(
          delay(500)
        )
        .subscribe((valor) => {
          if (valor === 'update') {
            this.cargarLibros();
          }
        });
    }
  }



  openDialog2(valor: Usuario) {
    if (screen.width < 500) {
      this.dialog
        .open(UsuarioComponent, {
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarLibros();
          }
        });
    } else {
      this.dialog
        .open(UsuarioComponent, {
          width: '60%',
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarLibros();
          }
        });
    }
  }

  habilitarLibro(valor: Libro) {
    if (screen.width < 500) {
      this.dialog
        .open(HabilitarLibroComponent, {
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarLibros();
          }
        });
    } else {
      this.dialog
        .open(HabilitarLibroComponent, {
          width: '60%',
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarLibros();
          }
        });
    }
  }


  imagenLibro(valor: Libro) {
    if (screen.width < 500) {
      this.dialog
        .open(ModalImagenComponent, {
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarLibros();
          }
        });
    } else {
      this.dialog
        .open(ModalImagenComponent, {
          width: '60%',
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarLibros();
          }
        });
    }
  }



  borrarLibro(valor: string){
    console.log('ABRIR PREGUNTA');
    
    return Swal.fire({
      title: 'Borrar Libro?',
      text: `Esta a punto de Borrar ${valor}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si Deseo Borrar Mi Libro'
    }).then((result) => {
      if (result.value) {
        this.libroService.eliminarLibro(valor)
        .subscribe({
          next: () =>{
            this.cargarLibros();
            Swal.fire(
              'Libro Borrado',
              `El Libro ${valor} fue eliminado Correctamente`,
              'success'
            )
          },
          error: () => {
            Swal.fire(
              'Error',
              `Ocurrio un Error`,
              'error'
            )
          }})
      }
    })
  }





}



















































// getAll() {
//   this._librosSVC.getOneData(this.userName).subscribe((res) => {
//     this.readData = res.data;
//     if (this.readData === undefined) {
//       this.tabla = false;
//     }
//   });
// }
// guardar() {
//   console.log('guardar');
//   let nombreImagen = this.formulario.value.imagen;
//   const imgName = nombreImagen.split('\\');
//   const dividir = imgName[2].split('.');
//   const extension = this.tipo_archivo.split('/');
//   if (this.formulario.valid) {
//     console.log(this.formulario.value);
//     let hora = new Date();
//     this.Form = {
//       precio: this.formulario.value.precio,
//       hora_publicacion: hora,
//       estado: this.formulario.value.estado,
//       id_categoria: this.formulario.value.id_categoria,
//       formato: this.formulario.value.formato,
//       ubicacion: 'Cochabamba, borrar este campo',
//       idioma: this.formulario.value.idioma,
//       autor_nombre: this.formulario.value.autor_nombre,
//       autor_apellido: this.formulario.value.autor_apellido,
//       anio_publicacion: this.formulario.value.anio_publicacion,
//       edad_publico: this.formulario.value.edad_publico,
//       numero_paginas: this.formulario.value.numero_paginas,
//       titulo: this.formulario.value.titulo,
//       subtitulo: this.formulario.value.subtitulo,
//       descripcion: this.formulario.value.descripcion,
//       imagen1: `../../../../assets/img/libros/${dividir[0]}.${dividir[1]}`,
//       tipo1: `.${extension[1]}`,
//       pago: 'false',
//       fecha_limite: '',
//       id_vendedor: this.userName,
//     };
//     this.onFileUpload();
//     this.mensaje = this.enviado;
//     this.limpiarFormulario();
//     setTimeout(() => {
//       this.mensaje = '';
//     }, 4000);
//     this.Snack.error = true;
//     setTimeout(() => {
//       this.Snack.error = false;
//     }, 4000);
//     this.propiedades.error = false;
//     this.createData(this.Form);
//   }
// }
// UpdatePerfil() {
//   console.log(this.formPerfil.value, this.readPerfil.id_vendedor);
//   this.editarUsuario(this.formPerfil.value, this.readPerfil.id_vendedor);
// }
// limpiarFormulario() {
//   this.formulario.reset();
// }
// getCategoria() {
//   this._librosSVC.getAllDataCate().subscribe((res) => {
//     this.readCategoria = res.data;
//   });
// }
// crearLibro(id: string) {
//   this.router.navigate([`/perfil/crear-libro/${id}`]);
// }
// onSelect(event: any) {
//   this.file = event.target.files[0];
//   console.log(this.file.type);
//   const filew = event.target.files[0];
//   this.tipo_archivo = filew.type;
//   if (event.target.files[0]) {
//     let reader = new FileReader();
//     reader.readAsDataURL(event.target.files[0]);
//     reader.onload = (event: any) => {
//       this.url = event.target.result;
//     };
//   }
// }
// onFileUpload() {
//   console.log('upload foto');
//   console.log(
//     'FILE INOUT',
//     this.fileInput.nativeElement.files[0],
//     'FILE INPUT'
//   );
//   const imageBlob = this.fileInput.nativeElement.files[0];
//   const file = new FormData();
//   console.log('prim', file, 'primero');
//   file.set('file', imageBlob);
//   this.http.post(`${this.apiUrlImg}`, file).subscribe((res) => {
//     console.log('segundo', file, 'CUARTO');
//   });
// }
// getPerfil() {
//   this._vendedorSVC.getOneData(this.userName).subscribe((res) => {
//     this.readPerfil = res.data[0];
//     console.log(this.readPerfil);
//   });
// }
// deleteData(id: any) {
//   console.log(id, 'deleteid==>');
//   this._librosSVC.deleteData(id).subscribe((res) => {
//     console.log(res, 'deleteRes ==>');
//     this.getAll();
//   });
// }
// editarData(id: any) {
//   this.router.navigate([`/perfil/crear-libro/${id}`]);
// }
// editarUsuario(data: any, id: number) {
//   console.log(data);

//   this._vendedorSVC.updateData(data, id).subscribe((res) => {
//     console.log('update');
//     this.propiedades.error = false;
//     this.getPerfil();
//   });
// }
// createData(data: any) {
//   this._librosSVC.createData(data).subscribe((res) => {
//     console.log(res, 'res create REgistro==>');
//     this.getAll();
//   });
// }
// editPerfil() {
//   this.ReadOnly = false;
// }


  // datoNoValido(campo: string) {
  //   return (
  //     this.formLibro.get(campo)?.invalid && this.formLibro.get(campo)?.touched
  //   );
  // }

  
  // crearFormularioLibro() {
  //   this.formLibro = this.fb.group({
  //     estado: [''],
  //     formato: [''],
  //     idioma: ['', Validators.required],
  //     autor_nombre: ['', [Validators.required, Validators.minLength(3)]],
  //     precio: ['', Validators.required],
  //     autor_apellido: ['', Validators.minLength(3)],
  //     anio_publicacion: [''],
  //     edad_publico: [''],
  //     numero_paginas: [''],
  //     titulo: ['', [Validators.required, Validators.minLength(3)]],
  //     subtitulo: ['', [Validators.minLength(3)]],
  //     descripcion: ['', [Validators.required, Validators.minLength(3)]],
  //     imagen: ['', Validators.required],
  //   });
  // }


  // crearFormularioUsuario() {
  //   this.perfilFormulario = this.fb.group({
  //     nombre: ['', [Validators.required, Validators.minLength(3)]],
  //     email: ['', [Validators.required, Validators.email, Validators.pattern(
  //       /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/
  //     )]],
  //     categoria: ['', Validators.required],
  //     role: ['', Validators.required],
  //     google: ['', Validators.required],
  //     coin: ['', Validators.required],
  //     celular: ['', [Validators.minLength(8), Validators.maxLength(8)]],
  //     celular2: ['', [Validators.minLength(8), Validators.maxLength(8)]],
  //     telefono: ['', [Validators.minLength(5), Validators.maxLength(8)]],
  //     telefono2: ['', [Validators.minLength(5), Validators.maxLength(8)]],
  //     page_web: ['', Validators.minLength(5)],
  //     direccion: ['', Validators.minLength(10)],
  //   });
  // }



  // this.perfilForm = this.fb.group({
    //   nombre: [this.usuario.nombre, Validators.required],
    //   celular: [this.usuario.celular],
    //   celular2: [this.usuario.celular2],
    //   telefono: [this.usuario.telefono],
    //   telefono2: [this.usuario.telefono2],
    //   direccion: [this.usuario.direccion],
    //   page_web: [this.usuario.page_web],
    // });

    // actualizarPerfil() {
  //   // Realizar una Validación por si el usuario
  //   // Envia lo que sea;
  //   this.usuarioService.actualizarPerfil(this.perfilForm.value).subscribe({
  //     next: (resp) => {
  //       //TODO Aca se actualiza automaticamente porque los objetod en javascript son pasados por referencia
  //       const {
  //         nombre,
  //         celular,
  //         celular2,
  //         telefono,
  //         telefono2,
  //         direccion,
  //         page_web,
  //       } = this.perfilForm.value;
  //       this.usuario.nombre = nombre;
  //       this.usuario.celular = celular;
  //       this.usuario.celular2 = celular2;
  //       this.usuario.telefono = telefono;
  //       this.usuario.telefono2 = telefono2;
  //       this.usuario.direccion = direccion;
  //       this.usuario.page_web = page_web;

  //       Swal.fire('Guardado', 'Tus Cambios Fueron Guardados', 'success');
  //     },
  //     error: (err) => {
  //       Swal.fire('Ocurrio un Error', err.error.msg, 'error');
  //     },
  //     complete: () => {
  //       console.log('Completado Correctamente');
  //     },
  //   });
  // }
  // tipo_archivo: any;
