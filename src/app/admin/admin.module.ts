import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';

import { AddEventoComponent } from './add-evento/add-evento.component';
import { AddComunicadoComponent } from './add-comunicado/add-comunicado.component';
import { AddAsociadoComponent } from './add-asociado/add-asociado.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';


// Angular-Material

import { AdminComponent } from './admin.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { MaterialModule } from './shared/material.module';
import { ModalUsuarioComponent } from './usuarios/modal-usuario/modal-usuario.component';
import { ModalEventoComponent } from './add-evento/modal-evento/modal-evento.component';
import { ModalComunicadoComponent } from './add-comunicado/modal-comunicado/modal-comunicado.component';
import { ModalAsociadoComponent } from './add-asociado/modal-asociado/modal-asociado.component';
import { AddCategoriaComponent } from './add-categoria/add-categoria.component';
import { ModalCategoriaComponent } from './add-categoria/modal-categoria/modal-categoria.component';
import { AyudaComponent } from './ayuda/ayuda.component';
import { AddPlantelComponent } from './add-plantel/add-plantel.component';
import { ModalPlantelComponent } from './add-plantel/modal-plantel/modal-plantel.component';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    AddEventoComponent,
    AddComunicadoComponent,
    AddAsociadoComponent,
    AdminComponent,
    UsuariosComponent,
    ModalUsuarioComponent,
    ModalEventoComponent,
    ModalComunicadoComponent,
    ModalAsociadoComponent,
    AddCategoriaComponent,
    ModalCategoriaComponent,
    AyudaComponent,
    AddPlantelComponent,
    ModalPlantelComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxPaginationModule,
    PipesModule
  ]
})
export class AdminModule { }
