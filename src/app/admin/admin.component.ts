import { Component, AfterViewInit, ViewChild, OnDestroy} from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { delay, filter } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { UsuarioService } from '../servicios/usuario.service';
import { Usuario } from '../models/usuario.model';
import { Subject } from 'rxjs/internal/Subject';
import { Subscription } from 'rxjs';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements AfterViewInit, OnDestroy{
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  routerEventSubscription!: Subscription;
  private destroy$: Subject<boolean> = new Subject<boolean>();

  public admin: Usuario;

  constructor(
            private observer: BreakpointObserver,
            private router: Router,
            private usuarioService: UsuarioService,
          ) {
            this.admin = this.usuarioService.usuario;
          }

  ngAfterViewInit() {
    this.observer
      .observe(['(max-width: 3000px)'])
      .pipe(delay(1))
      .subscribe((res) => {
        if (res.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.open();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });

      this.routerEventSubscription = this.router.events
      .pipe(
        filter((e) => e instanceof NavigationEnd)
      )
      .subscribe(() => {
        if (this.sidenav.mode === 'over') {
          console.log('Escucho la ruta');
          this.sidenav.close();
        }
      });
  }

  ngOnDestroy(): void {
    this.routerEventSubscription.unsubscribe();
    this.destroy$.next(true);
  }

  cerrarSession(){
    //TODO: RESOLVER
    this.usuarioService.logout();
    // Swal.fire({
    //   title: 'Esta Seguro?',
    //   text: "Cerrar Mi Sessión",
    //   icon: 'question',
    //   showCancelButton: true,
    //   confirmButtonColor: 'green',
    //   cancelButtonColor: 'black',
    //   confirmButtonText: 'Cerrar Sessión'
    // }).then((result) => {
    //   if (result.isConfirmed) {
    //     this.usuarioService.logout();
    //     Swal.fire({
    //       title:'Hasta Luego... 🙋‍♂️',
    //       text:'Sessión Cerrada Correctamente',
    //       icon:'success',
    //       timer: 1000,
    //   })
    //   }
    // });
  }
}