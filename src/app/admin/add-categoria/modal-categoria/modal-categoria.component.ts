import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/servicios/usuario.service';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoriaService } from 'src/app/servicios/categoria.service';

@Component({
  selector: 'app-modal-categoria',
  templateUrl: './modal-categoria.component.html',
  styleUrls: ['./modal-categoria.component.css'],
})
export class ModalCategoriaComponent implements OnInit {
  public categoriaForm!: FormGroup;
  hide = true;
  formSubmited: boolean = false;
  mensajeBoton: string = 'Crear Categoria';

  constructor(
    date: DateAdapter<Date>,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private dialogRef: MatDialogRef<ModalCategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any,
    private categoriaService: CategoriaService
  ) {
    date.getFirstDayOfWeek = () => 1;
  }

  ngOnInit(): void {
    this.categoriaForm = this.fb.group({
      categoria: ['', [Validators.required, Validators.minLength(3)]],
    });

    if (this.datoEdicion) {
      this.mensajeBoton = 'Guardar Cambios';
      this.categoriaForm.patchValue(this.datoEdicion);
    }
  }

  EditarCategoria() {
    if (!this.datoEdicion) {
      this.formSubmited = true;
      if (this.categoriaForm.invalid) {
        return;
      }
      this.categoriaService.crearCategoria(this.categoriaForm.value).subscribe({
        next: (res) => {
          this.categoriaForm.reset();
          this.dialogRef.close('save');
        },
        error: (error) => {
          alert('Error al Crear Categoria');
        },
      });
    } else {
      if (this.categoriaForm.invalid) {
        return;
      }  
      this.ActualizarCategoria();
    }
  }

  ActualizarCategoria() {
    
    this.categoriaService
      .actualizarCategoria(this.datoEdicion._id, this.categoriaForm.value)
      .subscribe({
        next: (res) => {
          this.categoriaForm.reset();
          this.dialogRef.close('update');
        },
        error: () => {
          alert('Error al Actualizar');
        },
      });
  }

  campoNoValido(campo: string): boolean {
    if (this.categoriaForm.get(campo)?.invalid && this.formSubmited) {
      return true;
    } else {
      return false;
    }
  }
}
