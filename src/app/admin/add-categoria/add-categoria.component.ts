import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalCategoriaComponent } from './modal-categoria/modal-categoria.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { CategoriaService } from 'src/app/servicios/categoria.service';
import Swal from 'sweetalert2';
import { Categoria } from 'src/app/models/categoria.model';

@Component({
  selector: 'app-add-categoria',
  templateUrl: './add-categoria.component.html',
  styleUrls: ['./add-categoria.component.css'],
})
export class AddCategoriaComponent implements OnInit, AfterViewInit {
  cargando: boolean = true;

  // pageActual: number = 1;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  displayedColumns: string[] = [
    'categoria',
    'nombre_usuario',
    'createdAt',
    'updatedAt',
    'accion',
  ];
  dataSource = new MatTableDataSource<any>();

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  constructor(
    private dialog: MatDialog,
    private categoriaService: CategoriaService
  ) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.cargarCategorias();
  }

  cargarCategorias() {
    this.cargando = true;
    this.categoriaService.cargarCategorias().subscribe(({ categorias }) => {
      this.dataSource = new MatTableDataSource(categorias);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cargando = false;
    });
  }

  editarCategoria(valor: Categoria) {
    if (screen.width < 500) {
      this.dialog
        .open(ModalCategoriaComponent, {
          maxWidth: '100vW',
          width: '80vW',
          height: '80vW',
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'update') {
            this.cargarCategorias();
          }
        });
    } else {
      this.dialog
        .open(ModalCategoriaComponent, {
          width: '60%',
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'update') {
            this.cargarCategorias();
          }
        });
    }
  }

  borrarCategoria(valor: string) {
    this.categoriaService.eliminarCategoria(valor).subscribe({
      next: () => {
        alert('Categoria Borrado Correctamente');
        this.cargarCategorias();
      },
      error: () => {
        alert('Error al Borrar La Categoria');
      },
    });
  }

  openDialog() {
    if (screen.width < 500) {
      this.dialog
        .open(ModalCategoriaComponent, {
          maxWidth: '100vW',
          width: '80vW',
          height: '80vW',
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarCategorias();
          }
        });
    } else {
      this.dialog
        .open(ModalCategoriaComponent, {
          width: '60%',
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarCategorias();
          }
        });
    }
  }
}
