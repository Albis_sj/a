import { Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Usuario } from 'src/app/models/usuario.model';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { ModalUsuarioComponent } from './modal-usuario/modal-usuario.component';
import { BusquedasService } from '../../servicios/busquedas.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
})
export class UsuariosComponent implements OnInit, AfterViewInit {
  // public totalUsuarios: number = 0;
  public usuarios: Usuario[] = [];
  // public usuariosTemp: Usuario[] = [];
  public desde: number = 0;
  public cargando: boolean = true;
  public imgSubs!: Subscription;
  usuario!: Usuario;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  displayedColumns: string[] = ['nombre', 'email', 'img', 'categoria', 'coin','page_web', 'celular', 'celular2','telefono', 'telefono2','direccion','createdAt', 'updatedAt','accion' ];
  dataSource = new MatTableDataSource<any>;


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  constructor(
    private dialog: MatDialog,
    private usuarioService: UsuarioService,
    private busquedaService: BusquedasService,
    private UsuarioService: UsuarioService,
  ) {
    this.usuario = this.usuarioService.usuario;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.cargarUsuarios();
  }



  cargarUsuarios() {
    this.cargando = true;
    this.usuarioService
      .cargarUsuarios(this.desde)
      //aca esta desestructurando la respuesta
      .subscribe(({ total, usuarios }) => {

        this.dataSource = new MatTableDataSource(usuarios);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        // this.totalUsuarios = total;
        // this.usuarios = usuarios;
        // this.usuariosTemp = usuarios;
        this.cargando = false;
        
      });
  }


  editarUsuario(valor: Usuario){
    if (screen.width < 500) {
      this.dialog.open(ModalUsuarioComponent, {
        maxWidth: '100vW',
        width: '80vW',
        height: '80vW',
        data: valor,
      }).afterClosed().subscribe(valor => {
        if (valor === 'update') {
          this.cargarUsuarios();
        }
      });
    } else {
      this.dialog.open(ModalUsuarioComponent, {
        width: '60%',
        data: valor
      }).afterClosed().subscribe(valor => {
        if (valor === 'update') {
          this.cargarUsuarios();
        }
      });
    }
  }



  borrarUsuario(valor: string){
    if(valor === this.usuarioService.uid){
      return Swal.fire('Error','No puede borrarse a si mismo', 'error')
    }
    return Swal.fire({
      title: 'Borrar Usuario?',
      text: `Esta a punto de Borrar a ${valor}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si Deseo Borrar al Usuario'
    }).then((result) => {
      if (result.value) {
        this.usuarioService.eliminarUsuario(valor)
        .subscribe({
          next: () =>{
            this.cargarUsuarios();
            Swal.fire(
              'Usuario Borrado',
              `El Usuario ${valor} fue eliminado Correctamente`,
              'success'
            )
          },
          error: () => {
            Swal.fire(
              'Error',
              `Ocurrio un Error`,
              'error'
            )
          }})
      }
    })
  }

  //TODO: HAY UNA POSIBILIDAD DE QUE EL ADMIN PUEDA CREAR UN USUARIO
  //QUE TENGA MUCHOS BENEFICIOS
  // cambiarPagina(valor: number) {
  //   this.desde += valor;
  //   if (this.desde < 0) {
  //     this.desde = 0;
  //   } else if (this.desde > this.totalUsuarios) {
  //     this.desde = valor;
  //   }
  //   this.cargarUsuarios();
  // }

  openDialog() {
    if (screen.width < 500) {
      this.dialog.open(ModalUsuarioComponent, {
        maxWidth: '100vW',
        width: '80vW',
        height: '80vW',
      });
    } else {
      this.dialog.open(ModalUsuarioComponent, {
        width: '60%',
      });
    }
  }
}

  // buscar(termino: string){
  //   if (termino.length === 0) {
  //     return this.usuarios = this.usuariosTemp;
  //   }
  //   return this.busquedaService.buscarU('usuarios',termino)
  //   .subscribe( resultados => {
  //     console.log(resultados)
  //     this.usuarios = resultados;
  //   });
  // }

  

  


