import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/servicios/usuario.service';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';



@Component({
  selector: 'app-modal-usuario',
  templateUrl: './modal-usuario.component.html',
  styleUrls: ['./modal-usuario.component.css'],
})
export class ModalUsuarioComponent implements OnInit {
  public usuariosForm!: FormGroup;
  hide = true;
  formSubmited: boolean = false;
  usuarios = ['Hola', 'Hola1', 'Hola 3'];
  mensajeBoton: string = 'Crear Usuario';

  constructor(
    date: DateAdapter<Date>,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private dialogRef: MatDialogRef<ModalUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any
  ) {
    date.getFirstDayOfWeek = () => 1;
    
  }

  ngOnInit(): void {
    this.usuariosForm = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', [Validators.required, Validators.email, Validators.pattern(
        /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/
      )]],
      categoria: ['', Validators.required],
      role: ['', Validators.required],
      google: ['', Validators.required],
      coin: ['', Validators.required],
      celular: ['', [Validators.minLength(8),Validators.maxLength(8)]],
      celular2: ['', [Validators.minLength(8),Validators.maxLength(8)]],
      telefono: ['',[Validators.minLength(5),Validators.maxLength(8)]],
      telefono2: ['',[Validators.minLength(5),Validators.maxLength(8)]],
      page_web: ['', Validators.minLength(5)],
      direccion: ['', Validators.minLength(10)],
    });


    if (this.datoEdicion) {
      this.mensajeBoton = 'Guardar Cambios';
      this.usuariosForm.patchValue(this.datoEdicion);
    }
  }

  EditarUsuario() {
    if (!this.datoEdicion) {
      this.formSubmited = true;
      if (this.usuariosForm.invalid) {
        return;
      }
      // this.usuarioService.cambiarUsuario();
      this.usuariosForm.reset();
      this.dialogRef.close('save');
      console.log(this.usuariosForm.value);
    } else {
      this.ActualizarUsuario();
    }
  }

  ActualizarUsuario(){
    if(!this.usuariosForm.valid){
      return;
    }
    this.usuarioService.cambiarUsuario(this.usuariosForm.value, this.datoEdicion.uid).subscribe({
      next: (res) => {
        console.log('Actualizado');
        this.usuariosForm.reset();
        this.dialogRef.close('update');
      },
      error: () => {
        alert('Error al Actualizar');
      }
    })
  }

  campoNoValido(campo: string): boolean {
    if (this.usuariosForm.get(campo)?.invalid && this.formSubmited) {
      return true;
    } else {
      return false;
    }
  }
}
