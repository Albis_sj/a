import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAsociadoComponent } from './modal-asociado.component';

describe('ModalAsociadoComponent', () => {
  let component: ModalAsociadoComponent;
  let fixture: ComponentFixture<ModalAsociadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAsociadoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModalAsociadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
