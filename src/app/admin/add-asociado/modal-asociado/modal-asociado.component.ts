import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/servicios/usuario.service';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComunicadoService } from 'src/app/servicios/comunicado.service';
import { AsociadoService } from 'src/app/servicios/asociado.service';

@Component({
  selector: 'app-modal-asociado',
  templateUrl: './modal-asociado.component.html',
  styleUrls: ['./modal-asociado.component.css']
})
export class ModalAsociadoComponent implements OnInit {

  public asociadosForm!: FormGroup;
  hide = true;
  formSubmited: boolean = false;
  usuarios = ['Hola', 'Hola1', 'Hola 3'];
  mensajeBoton: string = 'Crear Asociado';

  constructor(
    date: DateAdapter<Date>,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private dialogRef: MatDialogRef<ModalAsociadoComponent>,
    private comunicadoService: ComunicadoService,
    private asociadoService: AsociadoService,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any,
  ) {
    date.getFirstDayOfWeek = () => 1;
    
  }

  ngOnInit(): void {
    this.asociadosForm = this.fb.group({
      _id: [''],
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      representante: ['', [Validators.required, Validators.minLength(3)]],
      direccion: ['',[Validators.required, Validators.minLength(10)]],
      correo: [''],
      page_web: [''],
      celular: ['', [Validators.required]],
      telefono:[''],
      img: [''],
    });
    if (this.datoEdicion) {
      this.mensajeBoton = 'Guardar Cambios';
      this.asociadosForm.patchValue(this.datoEdicion);
    }

  }

  EditarAsociado() {
    if (!this.datoEdicion) {
      this.formSubmited = true;
      if (this.asociadosForm.invalid) {
        return;
      }
      this.asociadoService.crearAsociado(this.asociadosForm.value)
        .subscribe({
          next: () => {
            this.asociadosForm.reset();
            this.dialogRef.close('save');
          },
          error: () => {
            alert('Error al Crear')
          }
        })
    } else {
      this.ActualizarAsociado();
    }
  }

  ActualizarAsociado(){
    if(!this.asociadosForm.valid){
      return;
    }
    this.asociadoService.actualizarAsociado(this.datoEdicion._id, this.asociadosForm.value).subscribe({
      next: (res) => {
        console.log('Actualizado');
        this.asociadosForm.reset();
        this.dialogRef.close('update');
      },
      error: () => {
        alert('Error al Actualizar');
      }
    })
  }

  campoNoValido(campo: string): boolean {
    if (this.asociadosForm.get(campo)?.invalid && this.formSubmited) {
      return true;
    } else {
      return false;
    }
  }
}