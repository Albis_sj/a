import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAsociadoComponent } from './add-asociado.component';

describe('AddAsociadoComponent', () => {
  let component: AddAsociadoComponent;
  let fixture: ComponentFixture<AddAsociadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAsociadoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddAsociadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
