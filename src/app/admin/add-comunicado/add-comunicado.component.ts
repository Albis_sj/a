import { Component, OnInit, ViewChild,ElementRef, AfterViewInit} from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Usuario } from 'src/app/models/usuario.model';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { ModalComunicadoComponent } from './modal-comunicado/modal-comunicado.component';
import { BusquedasService } from '../../servicios/busquedas.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import Swal from 'sweetalert2';
import { ComunicadoService } from 'src/app/servicios/comunicado.service';
import { Comunicado } from 'src/app/models/comunicado.model';

@Component({
  selector: 'app-add-comunicado',
  templateUrl: './add-comunicado.component.html',
  styleUrls: ['./add-comunicado.component.css']
})
export class AddComunicadoComponent implements OnInit, AfterViewInit {
  // public totalUsuarios: number = 0;
  public comunicados: Comunicado[] = [];
  // public usuariosTemp: Usuario[] = [];
  public desde: number = 0;
  public cargando: boolean = true;
  public imgSubs!: Subscription;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  displayedColumns: string[] = ['titulo','descripcion','adjunto', 'img', 'accion'];
  dataSource = new MatTableDataSource<any>;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  constructor(
    private dialog: MatDialog,
    private usuarioService: UsuarioService,
    private busquedaService: BusquedasService,
    private comunicadoService: ComunicadoService,
  ) {
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.cargarComunicados();
  }

  cargarComunicados() {
    this.cargando = true;
    this.comunicadoService.cargarComunicados()
      .subscribe( ({comunicados})  => {
        console.log(comunicados);
        
        this.dataSource = new MatTableDataSource(comunicados);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.cargando = false;
        
      });
  }


  editarComunicado(valor: Usuario){
    if (screen.width < 500) {
      this.dialog.open(ModalComunicadoComponent, {
        maxWidth: '100vW',
        width: '80vW',
        height: '80vW',
        data: valor,
      }).afterClosed().subscribe(valor => {
        if (valor === 'update') {
          this.cargarComunicados();
        }
      });
    } else {
      this.dialog.open(ModalComunicadoComponent, {
        width: '60%',
        data: valor
      }).afterClosed().subscribe(valor => {
        if (valor === 'update') {
          this.cargarComunicados();
        }
      });
    }
  }



  borrarComunicado(valor: string){
    return Swal.fire({
      title: 'Borrar Comunicado?',
      text: `Esta a punto de Borrar ${valor}`,
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si Deseo Borrar el Comunicado'
    }).then((result) => {
      if (result.value) {
        this.comunicadoService.eliminarComunicado(valor)
        .subscribe({
          next: () =>{
            this.cargarComunicados();
            Swal.fire(
              'Comunicado Borrado',
              `El Comunicado ${valor} fue eliminado Correctamente`,
              'success'
            )
          },
          error: () => {
            Swal.fire(
              'Error',
              `Ocurrio un Error`,
              'error'
            )
          }})
      }
    })
  }

  //TODO: HAY UNA POSIBILIDAD DE QUE EL ADMIN PUEDA CREAR UN USUARIO
  //QUE TENGA MUCHOS BENEFICIOS
  // cambiarPagina(valor: number) {
  //   this.desde += valor;
  //   if (this.desde < 0) {
  //     this.desde = 0;
  //   } else if (this.desde > this.totalUsuarios) {
  //     this.desde = valor;
  //   }
  //   this.cargarUsuarios();
  // }

  openDialog() {
    if (screen.width < 500) {
      this.dialog.open(ModalComunicadoComponent, {
        maxWidth: '100vW',
        width: '80vW',
        height: '80vW',
      }).afterClosed().subscribe(valor => {
        if (valor === 'save') {
          this.cargarComunicados();
        }
      });
    } else {
      this.dialog.open(ModalComunicadoComponent, {
        width: '60%',
      }).afterClosed().subscribe(valor => {
        if (valor === 'save') {
          this.cargarComunicados();
        }
      });
    }
  }
}

  // buscar(termino: string){
  //   if (termino.length === 0) {
  //     return this.usuarios = this.usuariosTemp;
  //   }
  //   return this.busquedaService.buscarU('usuarios',termino)
  //   .subscribe( resultados => {
  //     console.log(resultados)
  //     this.usuarios = resultados;
  //   });
  // }

  

  








  
  
  // export interface comunicadoI{ 
  //   id_addComunicado: number;
  //   titulo: string,
  //   nombre_archivo: string,
  //   descripcion: string,
  //     imagen1: string,
  //     tipo1: string,
  //     imagen2?: string,
  //     tipo2?: string,
  // }




  // propiedades: any = {
  //   error: false
  // }

  // cantidadImg: number[] = [1];
  // number: number = 1;
  // tipo_archivo: any;

  // @ViewChild('fileInput', { static: false }) fileInput!: ElementRef
  // formulario!:FormGroup;
  // formOneData!: comunicadoI;
  // file!: File;
  // apiUrl = 'http://localhost:3000/addcomunicado';
  // apiUrlImg = 'http://localhost:3000/addComImg';
  // url = 'https://i.pinimg.com/736x/b5/49/41/b5494197b2d462c940f88988b203d290.jpg'
  // readData!: comunicadoI[];
  // rutaImg='../../../../assets/img/eventos/'

  // constructor(private fb: FormBuilder,
  //             private http: HttpClient,
  //             private _adminSVC: AdminService) { 

  //     this.crearFormulario();
  //     this.getInfo();
  //   }

  //   crearFormulario(){
  //     this.formulario = this.fb.group({
  //       titulo: ['', Validators.required],
  //       descripcion: ['', Validators.minLength(7)],
  //       imagenes: ['', Validators.required]
  //     })
  //    }
  
  
  //   ngOnInit(): void {
  //   }
  
  //   guardar(){
  //     console.log(this.formulario.value, 'PRIMERO');
  
  //     let nombreImagen = this.formulario.value.imagenes
  
  //       const imgName = nombreImagen.split('\\');
  //       console.log(imgName[2]);
  //       const dividir = imgName[2].split('.');
  //       console.log(dividir);
  
  //       const extension = this.tipo_archivo.split('/');
      
  //       this.formOneData = {
  //         id_addComunicado: 1,
  //         titulo: this.formulario.value.titulo,
  //         descripcion: this.formulario.value.descripcion,
  //         imagen1: `../../../../assets/img/comunicados/${imgName[2]}`,
  //         nombre_archivo: `${dividir[0]}`,
  //         tipo1: `.${extension[1]}`
  //       }
  
  //       console.log('SEGUNDO', this.formOneData, 'SEGUNDO');
        
  //       this.onFileUpload()
  //       this.createData(this.formOneData);
  //   }
  
  //     // On File Select
  //     onSelect(event: any) {
  //       this.file = event.target.files[0];
  //       console.log(this.file.type);
  //       const filew = event.target.files[0]
  //       this.tipo_archivo = filew.type
    
  //       if(event.target.files[0]){
  //         let reader = new FileReader();
  //         reader.readAsDataURL(event.target.files[0]);
  //         reader.onload = (event: any) => {
  //           this.url = event.target.result
  //         }
  //       }
  //     }
  
  //     onFileUpload(){
  //       console.log('upload foto TERVERO');
        
  //       const imageBlob = this.fileInput.nativeElement.files[0];
  //       const file = new FormData();
  //       console.log('TERCERO', file, 'TERCERP');
        
    
  //       file.set('file', imageBlob);
    
  //       this.http.post(`${this.apiUrlImg}`, file).subscribe(res => {
  //         // console.log(res);
  //         console.log('CUARTO', file, 'CUARTO');
          
  //       });
  //     }
  
  
  //     // IMAGENES
  //     adicionImg(){
  //       this.number = this.number +1
  //       this.cantidadImg.push(this.number)
  //       console.log(this.cantidadImg); 
  //     }
  
  //     borrarPasatiempo(i:number){
  //       // console.log('borrar', i);
  //     }
  
  //     // BASE DE DATOS
  //     // get all data
  //     getInfo(){
  //       this._adminSVC.getAllDataCom().subscribe((res)=>{
  //         console.log(res, "res==>");
  //         this.readData = res.data;
  //       });
  //     }
    
  //   //create data
  //   createData(data:any,){
  //     // console.log(data);
  //     this._adminSVC.createDataCom(data).subscribe((res)=>{
  //       console.log(res, 'res create Comuni ==>');
  //       this.getInfo();
  //     });
  //   }
  
  //     //delete data
  //     deleteData(id: any){
  //       console.log(id, 'deleteid==>');
  //       this._adminSVC.deleteDataCom(id).subscribe((res)=>{
  //         console.log(res, 'deleteRes ==>');
  //         this.getInfo();
  //       });
  //     }
  
  
  