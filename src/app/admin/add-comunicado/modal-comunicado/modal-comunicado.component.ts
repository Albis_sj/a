

import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/servicios/usuario.service';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ComunicadoService } from 'src/app/servicios/comunicado.service';

@Component({
  selector: 'app-modal-comunicado',
  templateUrl: './modal-comunicado.component.html',
  styleUrls: ['./modal-comunicado.component.css']
})
export class ModalComunicadoComponent implements OnInit {
  public comunicadosForm!: FormGroup;
  hide = true;
  formSubmited: boolean = false;
  usuarios = ['Hola', 'Hola1', 'Hola 3'];
  mensajeBoton: string = 'Crear Comunicado';

  constructor(
    date: DateAdapter<Date>,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private dialogRef: MatDialogRef<ModalComunicadoComponent>,
    private comunicadoService: ComunicadoService,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any,
  ) {
    date.getFirstDayOfWeek = () => 1;
    
  }

  ngOnInit(): void {
    this.comunicadosForm = this.fb.group({
      _id: [''],
      titulo: ['', [Validators.required, Validators.minLength(3)]],
      descripcion: ['', [Validators.required, Validators.minLength(3)]],
      adjunto: [''],
      img: [''],
    });


    if (this.datoEdicion) {
      this.mensajeBoton = 'Guardar Cambios';
      this.comunicadosForm.patchValue(this.datoEdicion);
    }
  }

  EditarComunicado() {
    if (!this.datoEdicion) {
      this.formSubmited = true;
      if (this.comunicadosForm.invalid) {
        return;
      }
      this.comunicadoService.crearComunicado(this.comunicadosForm.value)
        .subscribe({
          next: () => {
            this.comunicadosForm.reset();
            this.dialogRef.close('save');
          },
          error: () => {
            alert('Error al Crear')
          }
        })
    } else {
      this.ActualizarComunicado();
    }
  }

  ActualizarComunicado(){
    if(!this.comunicadosForm.valid){
      return;
    }
    this.comunicadoService.actualizarComunicado(this.datoEdicion._id, this.comunicadosForm.value).subscribe({
      next: (res) => {
        console.log('Actualizado');
        this.comunicadosForm.reset();
        this.dialogRef.close('update');
      },
      error: () => {
        alert('Error al Actualizar');
      }
    })
  }

  campoNoValido(campo: string): boolean {
    if (this.comunicadosForm.get(campo)?.invalid && this.formSubmited) {
      return true;
    } else {
      return false;
    }
  }
}