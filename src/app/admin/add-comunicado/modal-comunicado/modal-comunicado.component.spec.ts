import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalComunicadoComponent } from './modal-comunicado.component';

describe('ModalComunicadoComponent', () => {
  let component: ModalComunicadoComponent;
  let fixture: ComponentFixture<ModalComunicadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalComunicadoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModalComunicadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
