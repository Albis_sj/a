import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddAsociadoComponent } from './add-asociado/add-asociado.component';
import { AddComunicadoComponent } from './add-comunicado/add-comunicado.component';
import { AddEventoComponent } from './add-evento/add-evento.component';
import { AdminComponent } from './admin.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { AddCategoriaComponent } from './add-categoria/add-categoria.component';
import { AyudaComponent } from './ayuda/ayuda.component';

const routes: Routes = [


  {
    path: '',
    component: AdminComponent,
    children: [
      {path: '', component: AyudaComponent},
      {path: 'usuarios', component: UsuariosComponent},
      {path: 'comunicado', component: AddComunicadoComponent},
      {path: 'evento', component: AddEventoComponent},
      {path: 'asociado', component: AddAsociadoComponent},
      {path: 'categoria', component: AddCategoriaComponent},
      {path: 'ayuda', component: AyudaComponent},


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
