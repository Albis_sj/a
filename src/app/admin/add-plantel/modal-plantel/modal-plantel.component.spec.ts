import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalPlantelComponent } from './modal-plantel.component';

describe('ModalPlantelComponent', () => {
  let component: ModalPlantelComponent;
  let fixture: ComponentFixture<ModalPlantelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalPlantelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModalPlantelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
