import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlantelComponent } from './add-plantel.component';

describe('AddPlantelComponent', () => {
  let component: AddPlantelComponent;
  let fixture: ComponentFixture<AddPlantelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPlantelComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddPlantelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
