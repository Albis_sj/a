import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/servicios/usuario.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoriaService } from 'src/app/servicios/categoria.service';
import { EventoService } from 'src/app/servicios/evento.service';

@Component({
  selector: 'app-modal-evento',
  templateUrl: './modal-evento.component.html',
  styleUrls: ['./modal-evento.component.css']
})
export class ModalEventoComponent implements OnInit {
  public eventoForm!: FormGroup;
  hide = true;
  formSubmited: boolean = false;
  mensajeBoton: string = 'Crear Evento';

  constructor(
    date: DateAdapter<Date>,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private eventoService: EventoService,
    private dialogRef: MatDialogRef<ModalEventoComponent>,
    @Inject(MAT_DIALOG_DATA) public datoEdicion: any,
    private categoriaService: CategoriaService
  ) {
    date.getFirstDayOfWeek = () => 1;
  }

  ngOnInit(): void {
    this.eventoForm = this.fb.group({
      _id: [''],
      titulo: ['', [Validators.required, Validators.minLength(3)]],
      detalles: ['', [Validators.required, Validators.minLength(3)]],
      img: [''],
      fecha: [''],
    });

    if (this.datoEdicion) {
      this.mensajeBoton = 'Guardar Cambios';
      console.log(this.datoEdicion);
      this.eventoForm.patchValue(this.datoEdicion);
      console.log(this.eventoForm.value);
    }
  }

  EditarEvento() {
    if (!this.datoEdicion) {
      this.formSubmited = true;
      if (this.eventoForm.invalid) {
        return;
      }
      this.eventoService.crearEvento(this.eventoForm.value).subscribe({
        next: (res) => {
          this.eventoForm.reset();
          this.dialogRef.close('save');
        },
        error: (error) => {
          alert('Error al Crear Evento');
        },
      });
    } else {
      if (this.eventoForm.invalid) {
        return;
      }  
      this.ActualizarEvento();
    }
  }

  ActualizarEvento() {
    
    this.eventoService.actualizarEvento(this.eventoForm.value)
      .subscribe({
        next: (res) => {
          this.eventoForm.reset();
          this.dialogRef.close('update');
        },
        error: () => {
          alert('Error al Actualizar');
        },
      });
  }

  campoNoValido(campo: string): boolean {
    if (this.eventoForm.get(campo)?.invalid && this.formSubmited) {
      return true;
    } else {
      return false;
    }
  }
}
