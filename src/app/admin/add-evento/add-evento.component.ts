
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminService } from 'src/app/servicios/admin.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';




import { Component,ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalEventoComponent } from './modal-evento/modal-evento.component';
import Swal from 'sweetalert2';
import { EventoService } from 'src/app/servicios/evento.service';
import { Evento } from 'src/app/models/evento.model';
@Component({
  selector: 'app-add-evento',
  templateUrl: './add-evento.component.html',
  styleUrls: ['./add-evento.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AddEventoComponent implements AfterViewInit {
  cargando: boolean = true;

  // pageActual: number = 1;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  displayedColumns: string[] = [
    'titulo',
    'detalles',
    'img',
    'fecha',
    'createdAt',
    'updatedAt',
    'accion',
  ];
  dataSource = new MatTableDataSource<any>();

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  constructor(
    private eventoService: EventoService,
    private dialog: MatDialog,
  ) {}

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit(): void {
    this.cargarEventos();
  }

  cargarEventos() {
    this.cargando = true;
    this.eventoService.cargarEventos().subscribe(( eventos ) => {
      this.dataSource = new MatTableDataSource(eventos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.cargando = false;
    });
  }

  editarEvento(valor: Evento) {
    if (screen.width < 500) {
      this.dialog
        .open(ModalEventoComponent, {
          maxWidth: '100vW',
          width: '80vW',
          height: '80vW',
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'update') {
            this.cargarEventos();
          }
        });
    } else {
      this.dialog
        .open(ModalEventoComponent, {
          width: '60%',
          data: valor,
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'update') {
            this.cargarEventos();
          }
        });
    }
  }

  borrarEvento(valor: string) {
    this.eventoService.eliminarEvento(valor).subscribe({
      next: () => {
        alert('Evento Borrado Correctamente');
        this.cargarEventos();
      },
      error: () => {
        alert('Error al Borrar el Evento');
      },
    });
  }

  openDialog() {
    if (screen.width < 500) {
      this.dialog
        .open(ModalEventoComponent, {
          maxWidth: '100vW',
          width: '80vW',
          height: '80vW',
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarEventos();
          }
        });
    } else {
      this.dialog
        .open(ModalEventoComponent, {
          width: '60%',
        })
        .afterClosed()
        .subscribe((valor) => {
          if (valor === 'save') {
            this.cargarEventos();
          }
        });
    }
  }
}















/** Builds and returns a new User. */
// function createNewUser(id: number): UserData {
//   const name =
//     NAMES[Math.round(Math.random() * (NAMES.length - 1))] +
//     ' ' +
//     NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) +
//     '.';

//   return {
//     id: id.toString(),
//     name: name,
//     progress: Math.round(Math.random() * 100).toString(),
//     fruit: FRUITS[Math.round(Math.random() * (FRUITS.length - 1))],
//     description: FRUITS[Math.round(Math.random() * (FRUITS.length - 1))],
//   };
// }

//   propiedades: any = {
//     error: false
//   }
//   tipo_archivo: any;

//   @ViewChild('fileInput', { static: false }) fileInput!: ElementRef
//   formulario!:FormGroup;
//   formOneData!: FormI;
//   file!: File;
//   apiUrlImg = 'http://localhost:3000/addEventImg';
//   url = 'https://i.pinimg.com/736x/b5/49/41/b5494197b2d462c940f88988b203d290.jpg';
//   readData!: FormI[];
//   rutaImg='../../../../assets/img/eventos/'

//   constructor(private fb: FormBuilder,
//     private http: HttpClient,
//     private _adminSVC: AdminService) {
//       this.crearFormulario();
//       this.getInfo();
//     }

//     crearFormulario(){
//       this.formulario = this.fb.group({
//         titulo: ['', Validators.required],
//         descripcion: ['', Validators.minLength(7)],
//         imagenes: ['', Validators.required]
//       })
//      }

//   ngOnInit(): void {
//   }

//   guardar(){
//     console.log(this.formulario.value, 'PRIMERO');

//     let nombreImagen = this.formulario.value.imagenes

//       const imgName = nombreImagen.split('\\');
//       console.log(imgName);
//       const dividir = imgName[2].split('.');
//       console.log(dividir);

//       const extension = this.tipo_archivo.split('/');

//       this.formOneData = {
//         id_addEvent: 1,
//         titulo: this.formulario.value.titulo,
//         descripcion: this.formulario.value.descripcion,
//         imagen1: `../../../../assets/img/eventos/${dividir[0]}.${dividir[1]}`,
//         tipo1: `.${extension[1]}`
//       }

//       console.log('SEGUNDO', this.formOneData, 'SEGUNDO');

//       this.onFileUpload()
//       this.createData(this.formOneData);
//   }

//   // On File Select
//   onSelect(event: any) {
//     this.file = event.target.files[0]
//     console.log(this.file.type);
//     const filew = event.target.files[0]
//     this.tipo_archivo = filew.type

//     if(event.target.files[0]){
//       let reader = new FileReader();
//       reader.readAsDataURL(event.target.files[0]);
//       reader.onload = (event: any) => {
//         this.url = event.target.result
//       }
//     }
//   }

//   onFileUpload(){
//     console.log('upload foto TERVERO');

//     const imageBlob = this.fileInput.nativeElement.files[0];
//     const file = new FormData();
//     console.log('TERCERO', file, 'TERCERP');

//     file.set('file', imageBlob);

//     this.http.post(`${this.apiUrlImg}`, file).subscribe(res => {
//       // console.log(res);
//       console.log('CUARTO', file, 'CUARTO');

//     });
//   }

//   // BASE DE DATOS
//   // BASE DE DATOS

//   // get all data
//   getInfo(){
//     this._adminSVC.getAllDataEvent().subscribe((res)=>{
//       console.log(res, "res==>");
//       this.readData = res.data;
//     });
//   }

//   //create data
//   createData(data:any,){
//     console.log(data);
//     this._adminSVC.createDataEvent(data).subscribe((res)=>{
//       console.log(res, 'res create REgistro==>');
//       this.getInfo();
//     });
//   }

// //delete data
// deleteData(id: any){
//   console.log(id, 'deleteid==>');
//   this._adminSVC.deleteDataEvent(id).subscribe((res)=>{
//     console.log(res, 'deleteRes ==>');
//     this.getInfo();
//   });
// }
// }

// export interface FormI{
// id_addEvent: number
// titulo: string,
// descripcion: string,
//   imagen1: string,
//   tipo1: string,
//   imagen2?: string,
//   tipo2?: string,
//   imagen3?: string,
//   tipo3?: string,
//   imagen4?: string,
//   tipo4?: string,
//   imagen5?: string,
//   tipo5?: string,
//   imagen6?: string,
//   tipo6?: string,
//   imagen7?: string,
//   tipo7?: string,
//   imagen8?: string,
//   tipo8?: string,
//   imagen9?: string,
//   tipo9?: string,
//   imagen10?: string,
//   tipo10?: string,
// }
