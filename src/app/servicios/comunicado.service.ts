import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from '../../environments/environment';
const base_url = environment.base_url;

import * as moment from 'moment';
import { Comunicado } from '../models/comunicado.model';
// declare let moment: any; //declare moment
moment.locale('es');

@Injectable({
  providedIn: 'root'
})
export class ComunicadoService {

  constructor(private http: HttpClient) { }

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  convertir(fecha: any){
    // return moment(fecha).startOf('day').fromNow();
    return moment(fecha).startOf('minute').fromNow();
  }

  cargarComunicados(){
    return this.http.get<any>(`${base_url}/comunicados`, this.headers)
    .pipe(
      map((resp) => {
        const comunicados = resp.comunicados.map(
          (comunicado: any) =>
            new Comunicado(
              comunicado._id,
              comunicado.titulo,
              comunicado.descripcion,
              comunicado.adjunto,
              comunicado.img,
              this.convertir(comunicado.createdAt),
              this.convertir(comunicado.updatedAt),
            )
        )
        return {
          comunicados,
        };
      })
    );
  }

  crearComunicado( comunicado: Comunicado){
    return this.http.post(`${base_url}/comunicados`, comunicado,this.headers);
  }

  actualizarComunicado( _id: string, comunicado: Comunicado){
    //TODO: *CODIGO QUE SIRVE COMPLETAMENTE
    //PARA FILTRAR VALORES POR LLAVE DE UN OBJETO
  //   const users = {
  //     John: { username: 'johncam112', age:19 },
  //     Daniel: { key: 'Dandandel1', age:21 },
  //     Ruth: { key: 'rutie01', age:24 },
  //     Joe: { key: 'Joemathuel', age:28 }
  // };
  // const selectedUsers = ['Ruth', 'Daniel'];
  // const filteredUsers = Object.keys(users)
  //     .filter(key => selectedUsers.includes(key))
  //     .reduce((obj, key) => {
  //         obj[key] = users[key];
  //         return obj;
  //   }, {});
  // console.log(filteredUsers);
  // categoria = Object.keys(categoria)
  //     .filter(key => key.includes("categoria"))
  //     .reduce((obj: any, key) => {
  //         obj[key] = categoria[key];
  //         return obj;
  //   }, {});
    return this.http.put(`${base_url}/comunicados/${_id}`, comunicado ,this.headers);
  }

  eliminarComunicado( _id: string){
    return this.http.delete(`${base_url}/comunicados/${_id}`,this.headers);
  }
}