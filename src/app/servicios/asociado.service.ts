import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { environment } from '../../environments/environment';
const base_url = environment.base_url;

import * as moment from 'moment';
import { Asociado } from '../models/asociado.model';
// declare let moment: any; //declare moment
moment.locale('es');


@Injectable({
  providedIn: 'root'
})
export class AsociadoService {

  constructor(private http: HttpClient) { }

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  convertir(fecha: any){
    // return moment(fecha).startOf('day').fromNow();
    return moment(fecha).startOf('minute').fromNow();
  }

  cargarAsociados(){
    return this.http.get<any>(`${base_url}/asociados`, this.headers)
    .pipe(
      map((resp) => {
        const asociados = resp.asociados.map(
          (asociado: any) =>
            new Asociado(
              asociado._id,
              asociado.nombre,
              asociado.representante,
              asociado.direccion,
              asociado.correo,
              asociado.page_web,
              asociado.celular,
              asociado.telefono,
              asociado.img,
              this.convertir(asociado.createdAt),
              this.convertir(asociado.updatedAt),
            )
        )
        return {
          asociados,
        };
      })
    );
  }


  crearAsociado( asociado: Asociado){
    return this.http.post(`${base_url}/asociados`, asociado, this.headers);
  }

  actualizarAsociado( _id: string, asociado: Asociado){
    //TODO: *CODIGO QUE SIRVE COMPLETAMENTE
    //PARA FILTRAR VALORES POR LLAVE DE UN OBJETO
  //   const users = {
  //     John: { username: 'johncam112', age:19 },
  //     Daniel: { key: 'Dandandel1', age:21 },
  //     Ruth: { key: 'rutie01', age:24 },
  //     Joe: { key: 'Joemathuel', age:28 }
  // };
  // const selectedUsers = ['Ruth', 'Daniel'];
  // const filteredUsers = Object.keys(users)
  //     .filter(key => selectedUsers.includes(key))
  //     .reduce((obj, key) => {
  //         obj[key] = users[key];
  //         return obj;
  //   }, {});
  // console.log(filteredUsers);
  // categoria = Object.keys(categoria)
  //     .filter(key => key.includes("categoria"))
  //     .reduce((obj: any, key) => {
  //         obj[key] = categoria[key];
  //         return obj;
  //   }, {});
    return this.http.put(`${base_url}/asociados/${_id}`, asociado ,this.headers);
  }

  eliminarAsociado( _id: string){
    return this.http.delete(`${base_url}/asociados/${_id}`,this.headers);
  }
}