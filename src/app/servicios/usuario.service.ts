import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { catchError, map, Observable, of, tap } from 'rxjs';

import { environment } from '../../environments/environment';

import { CargarUsuarios } from '../interfaces/cargar-usuarios.interface';
import { LoginForm } from '../interfaces/login-form.interface';
import { RegisterForm } from '../interfaces/register-form.interface';

import { Usuario } from '../models/usuario.model';

import * as moment from 'moment';
// declare let moment: any; //declare moment
moment.locale('es');

const base_url = environment.base_url;
@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  public usuario!: Usuario;
  constructor(private _http: HttpClient, private router: Router) {
    this.usuario = new Usuario('', '', '', '', '', 0, false);
    // const d = moment().format('DD-MMM-YY');
    // console.log(d);
    // console.log(moment(new Date('2017-03-16')).format('dddd MMMM D'));
    // console.log(moment().format('MMMM Do YYYY, h:mm:ss a'));
    // console.log(moment().format('LLLL'));
  }

  convertir(fecha: any) {
    // return moment(fecha).startOf('day').fromNow();
    return moment(fecha).startOf('minute').fromNow();
  }
  get token(): string {
    return localStorage.getItem('token') || '';
  }

  get uid(): string {
    return this.usuario.uid || '';
  }

  get headers() {
    return {
      headers: {
        'x-token': this.token,
      },
    };
  }

  // get role(): 'ADMIN_ROLE' | 'USER_ROLE' {
  //   return this.usuario.role!;
  // }

  get role() {
    return this.usuario.role !== undefined ? this.usuario.role : '';
  }

  guardarLocalStorage(token: string, menu: any) {
    localStorage.setItem('token', token);
  }

  validarToken(): Observable<boolean> {
    return this._http
      .get(`${base_url}/login/renew`, {
        headers: {
          'x-token': this.token || '',
        },
      })
      .pipe(
        map((resp: any) => {
          const {
            nombre,
            email,
            categoria,
            role = '',
            google,
            coin,
            uid,
            img = '',
            celular,
            celular2,
            telefono,
            telefono2,
            page_web,
            direccion,
            createdAt,
            updatedAt,
          } = resp.usuario;

          this.usuario = new Usuario(
            nombre,
            email,
            '',
            categoria,
            role,
            coin,
            google,
            uid,
            img,
            celular,
            celular2,
            telefono,
            telefono2,
            page_web,
            direccion,
            this.convertir(createdAt),
            this.convertir(updatedAt)
          );
          //PARA VER LOS DETALLES DEL USUARIO
          // this.usuario.imprimirUsuario();
          this.guardarLocalStorage(resp.token, resp.menu);
          return true;
        }),
        //Este catch error atrapa el error del flujo y of devuelve
        //un nuevo Observable
        catchError((error) => of(false))
      );
  }

  crearUsuario(formData: RegisterForm) {
    console.log('Logre entrar al servicio');

    return this._http.post(`${base_url}/usuarios`, formData).pipe(
      tap((resp: any) => {
        this.guardarLocalStorage(resp.token, resp.menu);
        console.log('Logre guardar los datos');
      })
    );
  }
  actualizarPerfil(data: { nombre: string; email: string; celular: number; celular2: number; telefono: number; telefono2: number; direccion: string; page_web: string; categoria: string;role: string}) {
    //TODO el rol lo estaba extrayendo para que no se cambie
    data = {
      ...data,
      categoria: this.usuario.categoria,
      role: this.usuario.role,
      
    };

    return this._http.put(
      `${base_url}/usuarios/${this.uid}`,
      data,
      this.headers
    );
  }

  login(formData: LoginForm) {
    if (formData.remember) {
      localStorage.setItem('email', formData.email);
    } else {
      localStorage.removeItem('email');
    }

    return this._http.post(`${base_url}/login`, formData).pipe(
      tap((resp: any) => {
        //TODO Lo que esta comentado no esta en el codigo final de Fernando
        // localStorage.setItem('id', resp.usuarioDB.uid);
        this.guardarLocalStorage(resp.token, resp.menu);
        // localStorage.setItem('usuarioDB', JSON.stringify(resp.usuarioDB));
        // return true;
      }),
      catchError((error) => of(false))
    );
    //TODO No estoy seguro de implementar esto
  }

  loginGoogle(token: any) {
    return this._http.post(`${base_url}/login/google`, { token: token }).pipe(
      tap((resp: any) => {
        this.guardarLocalStorage(resp.token, resp.menu);
      })
    );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('menu');

    //TODO: RESOLVER
    this.usuario = new Usuario('', '', '', '', '', 0, false);
    // TODO BORRAR MENU
    this.router.navigate(['/']);
  }

  cargarUsuarios(desde: number = 0) {
    const url = `${base_url}/usuarios?desde=${desde}`;
    return this._http.get<CargarUsuarios>(url, this.headers).pipe(
      map((resp) => {
        const usuarios = resp.usuarios.map(
          //TODO ACA ESTAMOS INSTANCIANDO LA CLASE
          (user) =>
            new Usuario(
              user.nombre,
              user.email,
              '',
              user.categoria,
              user.role,
              user.coin,
              user.google,
              user.uid,
              user.img,
              user.celular,
              user.celular2,
              user.telefono,
              user.telefono2,
              user.page_web,
              user.direccion,
              this.convertir(user.createdAt),
              this.convertir(user.updatedAt)
            )
        );
        return {
          total: resp.total,
          usuarios,
        };
      })
    );
  }

  cambiarUsuario(usuario: Usuario, id: string) {
    return this._http.put(`${base_url}/usuarios/${id}`, usuario, this.headers);
  }

  eliminarUsuario(id: string) {
    const url = `${base_url}/usuarios/${id}`;
    return this._http.delete(url, this.headers);
  }
}

//   user!: VendedorI;
//   ID_user!: string;

//   constructor(private _http: HttpClient,
//     private router: Router) { }

//     apiUrl = 'http://localhost:3000/vendedor';
//   Vendedor!: VendedorLoginI;

//   //get all data
//   getAllData(): Observable<any>{
//     return this._http.get(`${this.apiUrl}`);
//   }

//   //get all data
//   getOneData(id: number): Observable<any>{
//     console.log(id, 0);
//     return this._http.get(`${this.apiUrl}/${id}`);
//   }

//   //create data
//   createData(data:any,):Observable<any>{
//     console.log(data, 'create Api Vendedor =>');

//     return this._http.post(`${this.apiUrl}`, data);
//   }

//   //update data
//   updateData(data:any, id: any):Observable<any>{
//     let ids = id;
//     // console.log('SERVICE',data, id, 'SERVICE');
//     // console.log(`${this.apiUrl}/${ids}`);

//     return this._http.put(`${this.apiUrl}/${ids}`, data);
//   }

//   //LOGIN DATOS
//   loginVendedor(user: VendedorI, id:number){
//     this.ID_user = id.toString()
//     this.user = user;
//     sessionStorage.setItem('ID', this.ID_user);
//     let nombre = sessionStorage.getItem('ID');

//     this.router.navigate([ '/perfil' ])
//     return  nombre;
//   }

// }

// export interface VendedorI {
//   nombre: string,
//   id_vendedor: number,
//   categoria: string,
//   correo1: string,
//   contrasenia: string,
//   telefono1: string,
//   telefono2: string,
//   celular1: number,
//   celular2: string,
//   correo2: number,
//   direccion: string,
//   pagina: string
// };

// export interface VendedorLoginI {
//   correo1: string,
//   contrasenia: string
// }
