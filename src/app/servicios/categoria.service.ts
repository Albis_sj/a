import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { Categoria } from '../models/categoria.model';
import { environment } from '../../environments/environment';
const base_url = environment.base_url;

import * as moment from 'moment';
// declare let moment: any; //declare moment
moment.locale('es');
@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(private http: HttpClient) { }

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  convertir(fecha: any){
    // return moment(fecha).startOf('day').fromNow();
    return moment(fecha).startOf('minute').fromNow();
  }

  cargarCategorias(){
    return this.http.get<any>(`${base_url}/categorias`, this.headers)
    .pipe(
      map((resp) => {
        const categorias = resp.categorias.map(
          //TODO ACA ESTAMOS INSTANCIANDO LA CLASE
          (categoria: any) =>
            new Categoria(
              categoria.categoria,
              categoria._id,
              this.convertir(categoria.createdAt),
              this.convertir(categoria.updatedAt),
              categoria.usuario._id,
              categoria.usuario.nombre,
              categoria.usuario.email,
            )
        )
        return {
          categorias,
        };
      })
    );
  }

  cargarCategorias2(){
    const url = `${base_url}/categorias`
    return this.http.get<any>(url, this.headers)
        .pipe(
          map( (resp: {
            ok: boolean,
            categorias: Categoria[]
          }) => resp.categorias )
        )
  }

  crearCategoria( categoria: Categoria){
    return this.http.post(`${base_url}/categorias`, categoria,this.headers);
  }

  actualizarCategoria( _id: string, categoria: Categoria){
    //TODO: *CODIGO QUE SIRVE COMPLETAMENTE
    //PARA FILTRAR VALORES POR LLAVE DE UN OBJETO
  //   const users = {
  //     John: { username: 'johncam112', age:19 },
  //     Daniel: { key: 'Dandandel1', age:21 },
  //     Ruth: { key: 'rutie01', age:24 },
  //     Joe: { key: 'Joemathuel', age:28 }
  // };
  // const selectedUsers = ['Ruth', 'Daniel'];
  // const filteredUsers = Object.keys(users)
  //     .filter(key => selectedUsers.includes(key))
  //     .reduce((obj, key) => {
  //         obj[key] = users[key];
  //         return obj;
  //   }, {});
  // console.log(filteredUsers);
  // categoria = Object.keys(categoria)
  //     .filter(key => key.includes("categoria"))
  //     .reduce((obj: any, key) => {
  //         obj[key] = categoria[key];
  //         return obj;
  //   }, {});
    return this.http.put(`${base_url}/categorias/${_id}`, categoria ,this.headers);
  }

  eliminarCategoria( _id: string){
    return this.http.delete(`${base_url}/categorias/${_id}`,this.headers);
  }
}