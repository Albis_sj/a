

import { HttpClient } from '@angular/common/http';
import { ElementRef, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs';
import { Libro } from '../models/libro.model';
import { environment } from 'src/environments/environment';
import { Evento } from '../models/evento.model';

const base_url = environment.base_url;
@Injectable({
  providedIn: 'root'
})

export class EventoService {

  constructor(private http: HttpClient) { }


  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  cargarEventos(){
    const url = `${base_url}/eventos`
    return this.http.get<any>(url, this.headers)
        .pipe(
          map( (resp: {
            ok: boolean,
            eventos: Evento[]
          }) => resp.eventos )
        )
  }

  obtenerEventoporId(id: string){
    const url = `${base_url}/eventos/${id}`
      return this.http.get<any>(url, this.headers)
        .pipe(
          map( (resp: {
            ok: boolean,
            evento: Evento
          }) => resp.evento )
        )
  }

  crearEvento( evento: Evento){
    const url = `${base_url}/eventos`
    return this.http.post(url, evento,this.headers);
  }

  actualizarEvento( evento: Evento){
    const url = `${base_url}/eventos/${evento._id}`
    return this.http.put(url,evento,this.headers);
  }

  eliminarEvento( _id: string){
    const url = `${base_url}/eventos/${_id}`
    return this.http.delete(url,this.headers);
  }
}


//   file!: File;
//   libro!: LibroI;
//   ListaLibro: LibroI[] = [];
//   ListaConFiltro:LibroI[] = [];
//   tabla: boolean = true;
//   userName!: number;

//   constructor(private _http: HttpClient) { 
//     this.getAllD();
//   }

//   apiUrl = 'http://localhost:3000/libros';
//   apiUrl_Un = 'http://localhost:3000/un-libro';
//   apiUrlCategoria = 'http://localhost:3000/libro-categoria'
//   apiUrlFormato = 'http://localhost:3000/libro-formato'
//   apiUrlBuscar = 'http://localhost:3000/libros_buscador';

//   Url = 'http://localhost:3000/libroEdi';
  
//   apiUrlImg = 'http://localhost:3000/addLibroImg';

//   getAllData(): Observable<any>{
//     return this._http.get(`${this.apiUrl}`);
//   }

//   //get one data
//   getOneData(id: number): Observable<any>{
//     console.log('ser', id);

//     return this._http.get(`${this.apiUrl}/${id}`);
//   }

  
//   //get one data
//   getLibroCategoria(id: number): Observable<any>{
//     console.log('ser', id);
//     return this._http.get(`${this.apiUrlCategoria}/${id}`);
//   }
//   getLibroFormato(formato: any): Observable<any>{
//     console.log(formato, 'formato 000');
//     return this._http.get(`${this.apiUrlFormato}/${formato}`)
//   }
//   getUnLibro(id: number): Observable<any>{
//     console.log('service', id);
//     return this._http.get(`${this.apiUrl_Un}/${id}`);
//   }
//   getAll(userName: number){
//     this.getAllData().subscribe((res)=>{
//       this.readData = res.data;
//       for (let i = 0; i < this.readData.length; i++){
//         if(this.readData[i].id_vendedor === userName){
//           this.ListaConFiltro.push(this.readData[i])
//           console.log( this.ListaConFiltro, 'libros del usuario');
//           this.ListaLibro = this.ListaConFiltro;
//         }
//         if(this.ListaConFiltro.length === 0){
//           this.tabla = false
//         }
//       }
//     });
//     this.getlibro();
//     return this.ListaLibro;
//   }
//   getlibro(){
//     console.log(this.ListaConFiltro);
//     return this.ListaLibro
//   }
//   createData(data:any):Observable<any>{
//     return this._http.post(`${this.apiUrl}`, data);
//   }
//   deleteData(id:any):Observable<any>{
//     let ids = id;
//     console.log(id, ids, 'service');
//     return this._http.delete(`${this.apiUrl}/${ids}`);
//   }
//   updateData(data:any, id: any):Observable<any>{
//     let ids = id;
//     console.log('SERVICE',data, id, 'SERVICE');
//     console.log(`${this.apiUrl}/${ids}`);
//     return this._http.put(`${this.apiUrl}/${ids}`, data);
//   }
//   getFormato(){
//     return this.formato
//   }
//   readData: any;
//   getAllD(){
//     this.getAllData().subscribe((res)=>{
//       this.readData = res.data;    
//     });
//   }
//   getLibros(){
//     this.getAllData().subscribe((res)=>{
//       this.readData = res.data;
//       this.ListaLibro = res.data;
//     });
//   }
//   getBuscar(termino: string): Observable<any>{
//     return this._http.get(`${this.apiUrlBuscar}/${termino}`);
//   }
//   buscarLibro(termino: string){
//     console.log(termino, 'librosvc');
//     let librosArr: LibroI[] = [];
//     this.getLibros()
//     for(let i = 0; i < this.ListaLibro.length; i++){
//       let libro: LibroI = this.ListaLibro[i];
//       console.log(libro);
//       let nombre = libro.titulo.toLowerCase();
//       console.log(nombre);
//       if(nombre.indexOf(termino) >= 0){
//         librosArr.push(libro);
//         console.log(librosArr, 'libros array');
//       }
//     }
//     return librosArr
//   }
//   onFileUpload(fileInput : ElementRef){
//     console.log('upload foto');
//     const imageBlob = fileInput.nativeElement.files[0];
//     const file = new FormData();
//     console.log('prim', file, 'primero');
//     file.set('file', imageBlob);
//     this._http.post(`${this.apiUrlImg}`, file).subscribe(res => {
//       console.log('segundo', file, 'CUARTO');
//     });
//   }
//   getLibroEdi(termino: any): Observable<any>{
//     console.log(termino);
//     return this._http.get(`${this.Url}/${termino}`);
//   }
//   apiUrlCate = 'http://localhost:3000/categoria';
//   getAllDataCate(): Observable<any>{
//     console.log('service para categoria');
//     return this._http.get(`${this.apiUrlCate}`);
//   }
// }


