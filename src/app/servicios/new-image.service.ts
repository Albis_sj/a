import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class NewImageService {

  async actualizarFoto(
    archivo: File, 
    tipo: 'usuarios'|'libros'|'hospitales',
    id: string){
    try {
      const url = `${base_url}/upload/${tipo}/${id}`;
      const formData = new FormData()
      formData.append('imagen', archivo);
      const resp = await fetch(url, {
        method: 'PUT',
        headers: {
          'x-token': localStorage.getItem('token') || ''
        },
        body: formData
      });
      const data = await resp.json();
      if(data.ok){
        Swal.fire('Felicidades', 'La imagen se subio', 'success');
        return data.nombreArchivo;
      } else {
        Swal.fire('Ocurrio un error', data.msg, 'error');
        return false;
      }
    } catch (err) {
      console.log(err, 'DEBES VER ESTE ERROR');
      return false;
    }
  }
}
