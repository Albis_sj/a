import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Libro } from 'src/app/models/libro.model';
import { Usuario } from '../models/usuario.model';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Categoria } from '../models/categoria.model';

const base_url = environment.base_url;


@Injectable({
  providedIn: 'root'
})
export class BusquedasService {

  constructor(private http: HttpClient) { }

  get token(): string{
    return localStorage.getItem('token') || '';
  }

  get headers(){
    return {
      headers: {
        'x-token': this.token
      }
    }
  }

  private transformarUsuarios(resultados: any[]): Usuario[]{
    return resultados.map(
      user => new Usuario(user.nombre,user.email,'',user.categoria,user.role,user.google,user.uid,user.img,user.celular,user.celular2,user.telefono,user.telefono2,user.page_web,user.direccion,user.creado,user.actualizado)
    );
  }

  buscarTodo(termino: string){
    const url = `${base_url}/todo/${termino}`
    return this.http.get(url, this.headers)
  }


  buscarU(
    tipo: 'usuarios'|'libros'|'hospitales',
    termino: string
    ){
    //TODO se puede implementar el paginado de la búsqueda pero hace falta en el backend
    //?desde=${desde}
    const url = `${base_url}/todo/coleccion/${tipo}/${termino}`
    return this.http.get<any[]>(url, this.headers).pipe(
          map((resp: any) => {
            switch (tipo) {
              case 'usuarios':
                return this.transformarUsuarios(resp.resultados);
              // case 'hospitales':
              //   return this.transformarHospitales(resp.resultados);
              default:
                return [];
            }
          })
        )
  }



}
