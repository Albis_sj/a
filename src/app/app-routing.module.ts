import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('./components/components.module').then((x) => x.ComponentsModule),
  },

  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((x) => x.AuthModule),
  },
  {
    path: 'perfil',
    canActivate: [AuthGuard],
    canLoad: [AuthGuard],
    loadChildren: () =>
      import('./perfil/perfil.module').then((x) => x.PerfilModule),
  },
  {
    path: 'admin',
    canActivate: [AdminGuard],
    canLoad: [AdminGuard],
    loadChildren: () =>
      import('./admin/admin.module').then((x) => x.AdminModule),
  },

  { path: '**', pathMatch: 'full', redirectTo: '' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true, //<- Indicar que se use el hash
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
