import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { UsuarioService } from '../servicios/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanLoad, CanActivate {
                            // CanActivate,

                            
  constructor(private usuarioService: UsuarioService,
              private router: Router){

  }
  canLoad(route: Route, segments: UrlSegment[]): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    return this.usuarioService.validarToken()
      .pipe(
        tap( estaAutenticado  => {
          if (estaAutenticado) {
            if(this.usuarioService.role === 'ADMIN_ROLE'){
              return true;
            } else {
                this.router.navigate(['/perfil']);
              
              return false;
            }
          } else {
            this.router.navigate(['/auth']);

            return false;
          }
        })
      )
  }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot){
      return this.usuarioService.validarToken()
      .pipe(
        tap( estaAutenticado  => {
          if (estaAutenticado) {
            if(this.usuarioService.role === 'ADMIN_ROLE'){
              return true;
            } else {
              this.router.navigate(['/perfil']);

              return false;
            }
          } else {
            this.router.navigate(['/auth']);
            
            return false;
          }
        })
      )
  }
  
}
