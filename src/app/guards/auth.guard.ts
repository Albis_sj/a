import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
  CanLoad,
  Route,
  UrlSegment,
} from '@angular/router';
import { Observable, tap } from 'rxjs';
import { UsuarioService } from '../servicios/usuario.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanLoad, CanActivate {
  // CanActivate,

  constructor(private usuarioService: UsuarioService, private router: Router) {}

  // Esto devolcia el CanLoad
  // :
  //   | boolean
  //   | UrlTree
  //   | Observable<boolean | UrlTree>
  //   | Promise<boolean | UrlTree> 

  canLoad(
    route: Route,
    segments: UrlSegment[]
  ){
    return this.usuarioService.validarToken().pipe(
      tap((estaAutenticado) => {
        if (estaAutenticado) {
          if(this.usuarioService.role === 'ADMIN_ROLE'){
            this.router.navigate(['/admin']);

            return true;
          } else {
            return false;
          }
        } else {
          this.router.navigate(['/auth']);

          return false;
        }
      })
    );
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
    return this.usuarioService.validarToken().pipe(
      tap((estaAutenticado) => {
        if (estaAutenticado) {
          if(this.usuarioService.role === 'ADMIN_ROLE'){
            this.router.navigate(['/admin']);
            return true;
          } else {
            return false;
          }
        } else {
          this.router.navigate(['/auth']);
          return false;
        }
      })
    );
  }
}
