import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/servicios/usuario.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})

export class RegisterComponent {
  @ViewChild('fileInput', { static: false }) fileInput!: ElementRef;
  file!: File;
  photoSelected!: string | ArrayBuffer | null;


  categoria: string[] = ['Autor', 'Editorial', 'Distribuidora'];

  public registerForm!: FormGroup;
  public formSubmited = false;


  constructor(
              private fb: FormBuilder,
              private usuarioService: UsuarioService,
              private router: Router,
            ) {
    this.crearFormulario();
  }

  crearFormulario(): void {
    this.registerForm = this.fb.group({
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(
            /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/
          ),
        ],
      ],
      password: ['', [Validators.required, Validators.minLength(4)]],
      categoria: ['', Validators.required],
      celular: ['',[Validators.minLength(8),Validators.maxLength(8)]],
      celular2: ['',[Validators.minLength(8),Validators.maxLength(8)]],
      telefono: ['',[Validators.minLength(5)]],
      telefono2: ['',[Validators.minLength(5)]],
      page_web: ['',Validators.minLength(5)],
      direccion: ['',Validators.minLength(10)],
    });
  }

  crearUsuario() {
    this.formSubmited = true;
    if(this.registerForm.invalid){
      console.log('No puedes ingresar');
      
      return;
    }

    this.usuarioService.crearUsuario(this.registerForm.value)
      .subscribe({
        next: (resp) => {
          Swal.fire({
            title: 'Bienvenido(a)!',
            text: 'Inicia Session con tus Credenciales',
            icon: 'success',
            // timer: 4000,
            // timerProgressBar: true,
            position: 'top-end',
          });
          this.router.navigate(['/auth']);
        },
        error: (err) => {
          Swal.fire('Opps',err.error.msg,'error');
        },
        complete: () => {
          console.log('Registro Exitoso');
        }
      });
  }

  campoNoValido(campo: string): boolean{
    if(this.registerForm.get(campo)?.invalid && this.formSubmited){
      return true;
    }else{
      return false;
    }
  }
}



// [disabled]="!registerForm.valid" [class.btn-invalid]="registerForm.valid"
  // On File Select
  // onSelect(event: any) {
  //   this.file = event.target.files[0];

  //   if (event.target.files[0]) {
  //     let reader = new FileReader();
  //     reader.readAsDataURL(event.target.files[0]);
  //     reader.onload = (event: any) => {
  //       this.url = event.target.result;
  //     };
  //   }
  // }




// // VALIDACIONES
// get nombreNoValido() {
//   return (
//     this.formulario.get('nombre')?.invalid &&
//     this.formulario.get('nombre')?.touched
//   );
// }

// get categoriaNoValido() {
//   return (
//     this.formulario.get('categoria')?.invalid &&
//     this.formulario.get('categoria')?.touched
//   );
// }

// get correo1NoValido() {
//   return (
//     this.formulario.get('correo1')?.invalid &&
//     this.formulario.get('correo1')?.touched
//   );
// }

// get contraseniaNoValido() {
//   return (
//     this.formulario.get('contrasenia')?.invalid &&
//     this.formulario.get('contrasenia')?.touched
//   );
// }

// get telefono1NoValido() {
//   return this.formulario.get('telefono1')?.invalid;
// }

// get telefono2NoValido() {
//   return this.formulario.get('telefono2')?.invalid;
// }

// get celular1NoValido() {
//   return this.formulario.get('celular1')?.invalid;
// }

// get celular2NoValido() {
//   return this.formulario.get('celular2')?.invalid;
// }

// get correo2NoValido() {
//   return this.formulario.get('correo2')?.invalid;
// }

// get paginaNoValido() {
//   return this.formulario.get('pagina')?.invalid;
// }