import { AfterViewInit, Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/servicios/usuario.service';

import Swal from 'sweetalert2';
import {LoginForm} from '../../interfaces/login-form.interface'
declare var google: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public formSubmitted = false;
  loading: boolean = false;


  public loginForm = this.fb.group({
    email: [localStorage.getItem('email') || '', [Validators.required, Validators.email,Validators.pattern(
      /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/
    )] ],
    password: ['123456', Validators.required ],
    remember: [false, Validators.required]
  });

  constructor(private router: Router,
    private fb: FormBuilder,
    private usuarioService: UsuarioService,
    private ngZone: NgZone) {
    }

  ngOnInit(): void {
    
  }


  login(){
    this.formSubmitted = true;

    if(!this.loginForm.valid){
      return;
    }
    let form: LoginForm = {
      email: this.loginForm.value.email!,
      password: this.loginForm.value.password!,
      remember: this.loginForm.value.remember!
    }

    this.usuarioService.login(form)
    .subscribe(
      {
        next: (resp) => {
          const {direct} = resp;
          (direct === 'ADMIN_ROLE') ? this.router.navigate(['/admin']) : this.router.navigate(['/perfil']);
        },
        error: (err) => {
          //Si sucede un Error
          // console.warn(err.error.msg)
          console.log(err)
          Swal.fire('Error', err.error.msg ,'error');
        },
        complete: () => {
          console.log('Completado');
        }
      });
  }


  fakeLoading():void{
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 3000);
  }

  ngAfterViewInit(): void {
    google.accounts.id.disableAutoSelect();
    google.accounts.id.initialize({
      //Esto lo cambie por mi id de google
      // client_id: "236025958894-l05tha7iovc0ool81upch4i6gi91npe8.apps.googleusercontent.com",
      client_id: "574602762860-h93ttcdj78s17gnc97kcrf0olhf1q1ur.apps.googleusercontent.com",
      callback: (response: any) => this.handleGoogleSignIn(response),
    });

    google.accounts.id.renderButton(
      document.getElementById("buttonDiv"),
      { size: "large", type: "standard", shape: "rectangular" }  // customization attributes
    );
  }

  handleGoogleSignIn(response: any) {
    console.log(response.credential);

    this.usuarioService.loginGoogle( response.credential )
            .subscribe(
              {
                next: (resp) => {
                  this.ngZone.run(() => {
                  this.router.navigate(['/perfil']);
                })
                },
                error: (err) => console.log('Debes verificar los campos o la ruta del servidor'),
              }
            );
    // This next is for decoding the idToken to an object if you want to see the details.
    let base64Url = response.credential.split('.')[1];
    let base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    let jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    console.log(JSON.parse(jsonPayload));
  }

  ngOnDestroy(): void {
    google.accounts.id.disableAutoSelect();
  }

  campoNoValido(campo: string): boolean{
    if(this.loginForm.get(campo)?.invalid && this.formSubmitted){
      return true;
    }else{
      return false;
    }
  }
}

  // mensaje!: string;
  // mensaje1: string = 'Ingresado';
  // noenviado: string = 'Correo o contraseña incorrecta'
  // propiedades: any = {
  //   error: false
  // }

  // formulario!: FormGroup;
  // loading: boolean = false;

  // constructor(private fb: FormBuilder,
  //   private _vendedorSVC: UsuarioService,
  //   private router: Router) { 
  //     this.crearFormulario()
  //   }

    
  // get correo1NoValido(){
  //   return this.formulario.get('correo1')?.invalid && this.formulario.get('correo1')?.touched
  // }

  // get contraseniaNoValido(){
  //   return this.formulario.get('contrasenia')?.invalid && this.formulario.get('contrasenia')?.touched
  // }

  // crearFormulario(): void {

  //   this.formulario = this.fb.group({
  //     correo1: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
  //     contrasenia: ['', [Validators.required, Validators.minLength(4)]],
  //     // recuerdame: ['']
  //   });
  // }

  // // variable con toda la informacion
  // readData:any;

  // ngOnInit(): void {
  // }

  // ingresar(){

  //   console.log(this.formulario.value);
  //   const Vendedor: VendedorLoginI = {
  //     correo1: this.formulario.value.correo1,
  //     contrasenia: this.formulario.value.contrasenia
  //   }

  //   this.getAllData(this.formulario.value.correo1, this.formulario.value.contrasenia)


  // }

  // getAllData(correo: string, contrasenia: string){
    
  //   this._vendedorSVC.getAllData().subscribe((res)=>{
  //     // console.log(res, "res==>");
  //     this.readData = res.data;    
  //     // console.log(this.readData); //TODOS LOS VENDEDORES

  //     for (let i = 0; i < this.readData.length; i++){
  //       // console.log(this.readData[i]);//LOG DE CADA VENDEDOR  

  //       if(this.readData[i].correo1 === correo && this.readData[i].contrasenia === contrasenia){

  //         this.fakeLoading();

  //         console.log(this.readData[i],'info del vendedor logeado');
  //         const here = this.readData[i];
  //         let gID = here.id_vendedor
          
  //         this._vendedorSVC.loginVendedor(this.readData[i], gID)

  //       } else if (this.readData[i].correo1 ==! correo) {

  //         this.mensaje = this.mensaje1;
  //         setTimeout(() => {
  //           this.mensaje = '';
  //         }, 4000);
    
  //       this.propiedades.error = true;
  //       setTimeout(() => {
  //         this.propiedades.error =false;
  //       }, 4000);

  //       } else {
  //         this.mensaje = this.noenviado;
  //         setTimeout(() => {
  //           this.mensaje = '';
  //         }, 4000);
    
  //       this.propiedades.error = true;
  //       setTimeout(() => {
  //         this.propiedades.error =false;
  //       }, 4000);

  //       this.formulario.reset();
  //       }
  //     }
  //   });
  // }

  // fakeLoading():void{
  //   this.loading = true;
    
  //   setTimeout(() => {
  //     this.loading = false;
  //     //Redireccionamos al dashboard
  //     // this.router.navigate(['index'])
  //   }, 3000);
  // }
