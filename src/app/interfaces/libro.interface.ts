export interface LibroI {
  _id?: string;
  titulo: string;
  descripcion: string;
  autor: string;
  precio: number;
  formato: string;
  idioma: string;
	usuario: string;
  categoria: string;
  habilitado: boolean;
  date_publicacion?: string;
  subtitulo?: string;
  estado?: string;
  edad_publico?: string;
  n_paginas?: number;
  img?: string;
  fecha_limite?: string;
  createdAt?: string;
  updatedAt?: string;
}
