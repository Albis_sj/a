export interface RegisterForm {
	nombre: string;
	email: string;
	password: string;
	categoria: string,
	celular?: number;
	celular2?: number;
	telefono?: number;
	telefono2?: number;
	page_web?: string;
	direccion?: string;
}