export class Categoria {
  constructor(
    public categoria: string,
    public _id?: string,
    public createdAt?: string,
    public updatedAt?: string,
    public id_usuario?: string,
    public nombre_usuario?: string,
    public correo_usuario?: string
  ) {}
}
