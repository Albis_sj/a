export class Comunicado {
  constructor(
    public _id: string,
    public titulo: string,
    public descripcion: string,
    public adjunto?: string,
    public img?: string,
    public createdAt?: string,
    public updatedAt?: string,
  ) {}
}
