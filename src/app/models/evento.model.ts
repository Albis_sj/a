export class Evento {
  constructor(
    public _id: string,
    public titulo: string,
    public detalles: string,
    public img?: string,
    public fecha?: Date,
    public createdAt?: string,
    public updatedAt?: string,
  ) {}
}
