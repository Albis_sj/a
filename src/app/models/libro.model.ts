import { Categoria } from './categoria.model';
interface LibroUser {
  _id: string;
  nombre: string;
  img: string;
  celular?: number;
}

export class Libro {
  constructor(
    public _id: string,
    public usuario: LibroUser,
    public categoria: Categoria,
    public titulo: string,
    public descripcion: string,
    public autor: string,
    public precio: number,
    public formato: string,
    public idioma: string,
    public habilitado: boolean,
    public date_publicacion?: number,
    public fecha_limite?: string,
    public img?: string,
    public subtitulo?: string,
    public estado?: string,
    public edad_publico?: string,
    public n_paginas?: string,
    public createdAt?: string,
    public updatedAt?: string
  ) {}
}
