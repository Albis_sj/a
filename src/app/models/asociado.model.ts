export class Asociado {
  constructor(
    public _id: string,
    public nombre: string,
    public representante: string,
    public direccion: string,
    public correo?: string,
    public page_web?: string,
    public celular?: number,
    public telefono?: number,
		public img?: string,
		public createdAt?: string,
		public updatedAt?: string,
  ) {}
}