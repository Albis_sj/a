import { environment } from "src/environments/environment";

const base_url = environment.base_url;

export class Usuario {
	constructor(
		public nombre: string,
		public email: string,
		public password: string,
		public categoria: string,
		// public role: 'ADMIN_ROLE' | 'USER_ROLE',
		public role: string,
		public coin: number,
		public google: boolean,
		public uid?: string,
		public img?: string,
		//TODO Esto es solo como decirle que sera uno de esos tipos
		public celular?: number,
		public celular2?: number,
		public telefono?: number,
		public telefono2?: number,
		public page_web?: string,
		public direccion?: string,
		public createdAt?: string,
		public updatedAt?: string,
	){

	}

	imprimirUsuario(){
		console.log(this.nombre);
		console.log(this.email);
		console.log(this.password);
		console.log(this.categoria);
		console.log(this.role);
		console.log(this.google);
		console.log(this.uid);
		console.log(this.img);
		console.log(this.coin);
		console.log(this.celular);
		console.log(this.celular2);
		console.log(this.telefono);
		console.log(this.telefono2);
		console.log(this.page_web);
		console.log(this.direccion);
		console.log(this.createdAt);
		console.log(this.updatedAt);
	}

	get imageUrl(){
		// /upload/usuario/no-image example

		if(!this.img){
			return `${base_url}/upload/usuarios/usuario`;
		} else if (this.img.toString().includes('https')){
			return this.img;
		} else if (this.img){
			return `${base_url}/upload/usuarios/${this.img}`;
		} else {
			return `${base_url}/upload/usuarios/usuario`;
		}
	}
}